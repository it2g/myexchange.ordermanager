﻿namespace Abp.RemoteEventBus
{
    public interface IHasTopicRemoteEventData
    {
        string Topic { get; set; }
    }
}
