﻿using Abp.RemoteEventBus;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto.OpenedOrder;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Exchange;
using MyExchange.Api.Services.Crud;
using MyExchange.Api.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace MyExchange.OrderManager.Api
{
    public class PlacedOrdersContainer : IPlacedOrdersContainer
    {
        private IList<PlacedOrder> _container;
        private IOpenedOrderCrudAppService _openedOrderCrudAppService;
        private ITradeCrudAppService _tradeCrudAppService;
        private readonly IOrderCrudAppService _orderCrudAppService;
        private static object _locker1;
        private static object _locker2;
        private IExchangeProvider _exchangeProvider;
        private IExchangeDriver _driver;
        private readonly ITimer _timer;
        private readonly IRemoteEventBus _remoteEventBus;



        public PlacedOrdersContainer(
            [NotNull] ITradeCrudAppService tradeCrudAppService,
            [NotNull] ITimer timer,
            [NotNull] IExchangeProvider exchangeProvider,
            [NotNull] IExchangeDriver driver,
            [NotNull] IOrderCrudAppService orderCrudAppService,
            [NotNull] IOpenedOrderCrudAppService openedOrderCrudAppService,
            [NotNull] IList<PlacedOrder> container,
            [NotNull] IRemoteEventBus remoteEventBus)
        {
            _tradeCrudAppService = tradeCrudAppService ?? throw new ArgumentNullException(nameof(tradeCrudAppService));
            _timer = timer ?? throw new ArgumentNullException(nameof(timer));
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _driver = driver ?? throw new ArgumentNullException(nameof(driver));
            _orderCrudAppService = orderCrudAppService ?? throw new ArgumentNullException(nameof(orderCrudAppService));
            _openedOrderCrudAppService = openedOrderCrudAppService ?? throw new ArgumentNullException(nameof(openedOrderCrudAppService));
            _container = container ?? throw new ArgumentNullException(nameof(container));
            _remoteEventBus = remoteEventBus ?? throw new ArgumentNullException(nameof(remoteEventBus));

        }

        public PlacedOrdersContainer([NotNull] IExchangeProvider exchangeProvider,
            [NotNull] IOpenedOrderCrudAppService openedOrderCrudAppService)
        {
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _openedOrderCrudAppService = openedOrderCrudAppService ?? throw new ArgumentNullException(nameof(openedOrderCrudAppService));
            _container = new List<PlacedOrder>();

        }

        public async Task Add(PlacedOrder order)
        {
            _container.Add(order);

            order.OpenedOrder.Order.OrderStatus = OrderStatuses.Placed;

            lock (_locker1)
            {
                var orderDto = _openedOrderCrudAppService.Update(order.OpenedOrder).Result;
            }

            var exchangeProvider = _exchangeProvider.Get(order.OpenedOrder.Order.ExchangeId);

            var orderCreateType = order.OpenedOrder.Order.TradeTypes == TradeTypes.Buy
                ? OrderCreateTypes.MarketBuy
                : OrderCreateTypes.MarketSell;

            order.OpenedOrder.Order.OrderId = await exchangeProvider.ExchangeDriver.OrderCreateAsync(order.OpenedOrder.Order, orderCreateType);

            lock (_locker2)
            {
                var orderDto = _orderCrudAppService.Update(order.OpenedOrder.Order).Result;
            }

        }

        public IList<PlacedOrder> GetAll()
        {
            return _container.ToList();
        }

        public async Task<IEnumerable<OpenedOrderDto>> CheckOrderForSendingAccountManagerAsync()
        {
            /*
             * Для реализации механизма отслеживания выставленных ордеров на бирже требуется:
               1. Сгруппировать ордера PlacedorderContainer по биржам и валютным парам
               2.Реализовать метод для выгрузки сделок пользователя с биржи
               2.1. Передать в качестве параметра валютные пары для выгрузки сделок пользователя
               2.2. сохранить полученные сделки в БД
               3. Выявить те ордера, которые закрылись сделками. Для этого осуществить сравнение orderID ордеров из 
               PlacedorderContainer с orderID сделок (исполненных ордеров)
               3.1. Сгруппировать сделки по orderID 
               3.2. Проходя по сгруппированным сделкам для каждого orderID находим ордер в контейнере и сравниваем количество закупленной валюты
               по сделкам с необходимым количеством по ордеру
               4. При выявлении закрытого ордера отправить в Kafka сообщение с ордером для Аккаунт менеджера. 
               5. Изменить статус ордера. Добавить статус ордера «Передан в AccountManager” и “Обработан AccountManager-ом”
               6. Удалить исполненный ордер из PlacedorderContainer
             */

            var listPlacedOrders = new List<PlacedOrder>();

            // 1. Сгруппировать ордера PlacedorderContainer по биржам и валютным парам
            var exchangePair = _container.GroupBy(source => source.OpenedOrder.Order.ExchangeId,
                elementSelector => elementSelector.OpenedOrder.Order.Pair);

            foreach (IGrouping<string, PairDto> pairs in exchangePair)
            {
                var exchangeId = pairs.Key;
                var exchange = _exchangeProvider.Get(exchangeId);
                // 2.1. Передать в качестве параметра список валютных пар для выгрузки сделок пользователя
                var userTrades = await exchange.ExchangeDriver.UserTradesAsync(pairs.Distinct());
                // 2.2. сохранить полученные сделки в БД
                var userTradesSQL = new List<TradeDto>();
                foreach (var trade in userTrades)
                {
                    var tradeDto = await _tradeCrudAppService.Update(trade);
                    userTradesSQL.Add(tradeDto);
                }

                // 3. Выявить те ордера, которые закрылись сделками. Для этого осуществить сравнение orderID ордеров из
                // PlacedorderContainer с orderID сделок(исполненных ордеров)

                //3.1.Сгруппировать сделки по orderID
                var groupedTradesByOrderId = userTradesSQL.GroupBy(t => t.ExternalOrderId);

                foreach (var tradesByOrderId in groupedTradesByOrderId)
                {
                    // 3.2. Проходя по сгруппированным сделкам для каждого orderID находим ордер в контейнере и сравниваем количество закупленной валюты
                    //      по сделкам с необходимым количеством по ордеру
                    var order = _container.FirstOrDefault(p => p.OpenedOrder.Order.OrderId == tradesByOrderId.Key
                                                               && p.OpenedOrder.Order.ExchangeId == exchangeId
                                                               && p.OpenedOrder.Order.Quantity == tradesByOrderId.Sum(t => t.Amount));

                    //Cписок ордеров найденных при сравнении со сделками
                    listPlacedOrders.Add(order);
                }
            }

            return listPlacedOrders.Select(p => p.OpenedOrder);
        }


        public Task CheckOrders()
        {
            throw new NotImplementedException();
        }

        public int Count()
        {
            throw new NotImplementedException();
        }

        public Task Remove(PlacedOrder order)
        {
            throw new NotImplementedException();
        }
    }

}
