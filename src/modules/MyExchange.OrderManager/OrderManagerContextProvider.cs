﻿using System;
using System.Threading.Tasks;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using JetBrains.Annotations;
using MyExchange.Api.Dto.OpenedOrder;
using MyExchange.Api.Services.Crud;
using MyExchange.OrderManager.Api;
using MyExchange.OrderManager.Api.Dal;
using System.Linq;
using System.Collections.Generic;
using MyExchange.Api.Services.Infrastructure;

namespace MyExchange.OrderManager.Dal.Repositories
{
    public class OrderManagerContextProvider : IOrderManagerContextProvider
    {
        private readonly ITrackedOrdersContainer _trackedOrdersContainer;
        private readonly IPlacedOrdersContainer _placedOrdersContainer;
        private readonly IOpenedOrderCrudAppService _openedOrderCrudAppService;
        private readonly ITimer _timer;

        public OrderManagerContextProvider(ITrackedOrdersContainer trackedOrdersContainer,
            [NotNull] IOpenedOrderCrudAppService openedOrderCrudAppService,
            [NotNull] ITimer timer,
            [NotNull] IPlacedOrdersContainer placedOrdersContainer)
        {
            _trackedOrdersContainer = trackedOrdersContainer;
            _placedOrdersContainer = placedOrdersContainer;
            _openedOrderCrudAppService = openedOrderCrudAppService ?? throw new ArgumentNullException(nameof(openedOrderCrudAppService));
            _timer = timer;
            _placedOrdersContainer = placedOrdersContainer ?? throw new ArgumentNullException(nameof(placedOrdersContainer));
        }

        public async Task<OrderManagerContext> GetContextAsync()
        {
            var orderManagerContext = new OrderManagerContext()
            {
                StartTime = _timer.CurrentTime()
                //Инициализируем другие свойства
            };
            //TODO  считывание настроек контекста из settings не касающиеся содержимого контейнеров ордеров

            orderManagerContext.TrackedOrdersContainer = _trackedOrdersContainer;
            orderManagerContext.PlacedOrdersContainer = _placedOrdersContainer;


            //Наполнение контейнеров ордерами
            var trackedOrders = (await _openedOrderCrudAppService.GetAll(new OpenedOrderPagedResultRequestDto
            { Status = MyExchange.Api.Constants.OrderStatuses.Tracked })).Items.ToList();

            trackedOrders.ForEach(o => _trackedOrdersContainer.Add(new TrackedOrder { OpenedOrder = o }));

            var placedOrders = (await _openedOrderCrudAppService.GetAll(new OpenedOrderPagedResultRequestDto
            { Status = MyExchange.Api.Constants.OrderStatuses.Placed })).Items;

            foreach (var order in placedOrders)
            {
                await _trackedOrdersContainer.Add(new TrackedOrder { OpenedOrder = order });
            }

            //TODO  сохранить настроки контекста в settings не касающиеся содержимого контейнеров ордеров

            return orderManagerContext;

        }
    }
}
