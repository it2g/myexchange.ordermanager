﻿using System;
using Abp.Dependency;
using Abp.RemoteEventBus;

namespace MyExchange.OrderManager.RemoteEventHandlers
{
    /// <summary>
    /// Это обработчик сообщений от Kafka</summary>
    [RemoteEventHandler(ForTopic = "order_for_order_manager", ForType = "order", Order = 1)]
    public class OrderManagerRemoteEventHandler : IOrderManagerRemoteEventHandler
    {
        public event EventHandler<IRemoteEventData> OrderManagerHandler;

        public void HandleEvent(RemoteEventArgs eventArgs)
        {
            if (eventArgs.EventData.Type.Equals("order"))
            {
                OrderManagerHandler(this, eventArgs.EventData);
            }
        }
    }

    public interface IOrderManagerRemoteEventHandler : IRemoteEventHandler, ISingletonDependency
    {
        event EventHandler<IRemoteEventData> OrderManagerHandler;
    }
}