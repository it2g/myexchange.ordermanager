﻿using System;
using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Dependency;
using JetBrains.Annotations;
using MyExchange.Api.Dto.OpenedOrder;
using MyExchange.Api.Dto.Signal;
using MyExchange.OrderManager.Api;

namespace MyExchange.OrderManager
{
    public class OrderManagerBuilder : ApplicationService, IOrderManagerBuilder
    {
        private readonly IOrderManager _orderManager;

        public OrderManagerBuilder([NotNull] IOrderManager orderManager)
        {
            _orderManager = orderManager ?? throw new ArgumentNullException(nameof(orderManager));
        }

        public IOrderManager Create(OrderManagerContext context)
        {
            _orderManager.Context = context;

            return _orderManager;
        }
    }
}