﻿using Abp.BackgroundJobs;
using Abp.RemoteEventBus;
using JetBrains.Annotations;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto.OpenedOrder;
using MyExchange.Api.OrderManagment;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;
using MyExchange.Api.Services.Infrastructure;
using MyExchange.Api.TradeExpert;
using MyExchange.Base.BotTactic;
using MyExchange.OrderManager.Api;
using MyExchange.OrderManager.RemoteEventHandlers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;

namespace MyExchange.OrderManager
{
    /// <summary>
    /// Менеджер ордеров.
    /// </summary>
    public class OrderManager : BotTacticBase<OrderManagerContext>, IOrderManager
    {
        private readonly IIndicatorProvider _indicatorProvider;
        private readonly IRemoteEventBus _remoteEventBus;
        private ITimer _timer;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly ITrailingStopCalculator _trailingStopCalculator;
        private readonly IBarProvider _barProvider;
        private readonly IPlacedOrdersContainer _placedOrdersContainer;
        private static object _locker1;
        private readonly IOrderCrudAppService _orderCrudAppService;


        public OrderManager([NotNull] ITimer timer,
            [NotNull] IOrderManagerRemoteEventHandler orderManagerRemoteEventHandler,
            [NotNull] IRemoteEventBus remoteEventBus,
            [NotNull] IIndicatorProvider indicatorProvider,
            [NotNull] IBackgroundJobManager backgroundJobManager,
            IOrderCrudAppService orderCrudAppService)
        {
            _remoteEventBus = remoteEventBus ?? throw new ArgumentNullException(nameof(remoteEventBus));
            _timer = timer ?? throw new ArgumentNullException(nameof(timer));
            _indicatorProvider = indicatorProvider ?? throw new ArgumentNullException(nameof(indicatorProvider));
            _backgroundJobManager = backgroundJobManager ?? throw new ArgumentNullException(nameof(backgroundJobManager));
            _orderCrudAppService = orderCrudAppService ?? throw new ArgumentNullException(nameof(orderCrudAppService));

            orderManagerRemoteEventHandler.OrderManagerHandler += HandleEvent;
        }

        public override string Name { get; } = "Менеджер ордеров";

        public override Action Execute => Managment;
        public override void Dispose()
        {

        }

        /// <summary>
        /// Собственный счетчик количества минут
        /// </summary>
        private int countMinuts = 0;

        //Список ТФ от 1 минуты до 1-го месяца. Представлен список в минутах.
        private int[] listTimeFrame = { 1, 5, 15, 30, 60, 240, 1440, 10080, 44640 };

        private async void Managment()
        {
            await _remoteEventBus.SubscribeAsync(KafkaTopics.GetEventBusTopicForOrderManager());

            var timer = new Timer(60000);

            timer.Elapsed += SelectAndReCalculateOrder;

            timer.Enabled = true;

            do
            {
                Console.WriteLine($"Принято ордеров {Context.TrackedOrdersContainer.Count()}");

                var trackedOrders = await Context.TrackedOrdersContainer.OrderActivationCheck();

                foreach (var trackedOrder in trackedOrders)
                {
                    await _placedOrdersContainer.Add(new PlacedOrder() { OpenedOrder = trackedOrder.OpenedOrder });
                    await Context.TrackedOrdersContainer.Remove(trackedOrder);
                }

                var openedOrderDtos = await Context.PlacedOrdersContainer.CheckOrderForSendingAccountManagerAsync();

                openedOrderDtos.ToList().ForEach(async o => await SendOrderForAccountManager(o));

                Thread.Sleep(_timer.SpeedUpInSeconds(300));

            } while (true);
        }

        /// <summary>
        /// С периодичность раз в минуту, таймер будет производить перерасчет стоп-лосса и тейк-профита ордеров
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SelectAndReCalculateOrder(object sender, ElapsedEventArgs e)
        {
            //При вызыве метода увеличиваем собственный счетчик времени на 1 минуту
            countMinuts++;
            //Увеличивать собственный счетчик времени в ОМ с ограничением в 1 месяц, потом сброс.
            //Я взял за снову 31 день, но надо подумать как быть, когда меньше дней в месяц
            if (countMinuts >= 44640)
                countMinuts = 0;

            var timeFrames = listTimeFrame.Where(x => countMinuts % x == 0);

            Context.TrackedOrdersContainer.ReCalculate(timeFrames);
        }

        public async Task SendOrderForAccountManager(OpenedOrderDto openedOrderDto)
        {
            //Отправляем в Кафка
            RemoteEventData data = new RemoteEventData("order", new Dictionary<string, object>
            {
                { "order", openedOrderDto },
            });

            //Изменить статус ордера
            openedOrderDto.Order.OrderStatus = OrderStatuses.TransferAccountManager;
            lock (_locker1)
            {
                var orderDto = _orderCrudAppService.Update(openedOrderDto.Order).Result;
            }


            await _remoteEventBus.PublishAsync(KafkaTopics.GetEventBusTopicForAccountManager(), data);
        }

        private TimeSpan GetTimeFrame(Stopwatch sw)
        {
            var result = sw.Elapsed;

            if (result > TimeSpan.FromDays(1))
            {
                sw.Restart();
            }

            return result;
        }


        /// <summary>
        /// Обрабатывает сигналы поступающие от торговых тактик на обслуживание ордеров
        /// </summary>
        /// <param name="eventArgs"></param>
        private void HandleEvent(object sendet, IRemoteEventData eventData)
        {
            if (eventData.Type == "order")
            {
                JObject jOrder = (JObject)eventData.Data["order"];

                OpenedOrderDto order = jOrder.ToObject<OpenedOrderDto>();

                Context.TrackedOrdersContainer.Add(new TrackedOrder { OpenedOrder = order });
            }
        }

    }
}
