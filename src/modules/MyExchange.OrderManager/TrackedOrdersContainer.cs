﻿using JetBrains.Annotations;
using MyExchange.Api.Constants;
using MyExchange.Api.OrderManagment;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;
using MyExchange.OrderManager.Api;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MyExchange.Api.Dto.OpenedOrder;
using Newtonsoft.Json;
using RestSharp;
using MyExchange.Api.Services.Infrastructure;

namespace MyExchange.OrderManager
{
    public class TrackedOrdersContainer : ITrackedOrdersContainer
    {
        private SortedSet<TrackedOrder> _container;
        private IOpenedOrderCrudAppService _openedOrderCrudAppService;
        private readonly IBarProvider _barProvider;
        private readonly ITrailingStopCalculator _trailingStopCalculator;
        private readonly IRestClient _restClient;
        private readonly ITimer _timer;

        private static string _baseUrl = "http://localhost:22022";

        public TrackedOrdersContainer([NotNull] IOpenedOrderCrudAppService openedOrderCrudAppService,
            [NotNull] IBarProvider barProvider,
            [NotNull] ITrailingStopCalculator trailingStopCalculator,
            [NotNull] IRestClient restClient,
            [NotNull] ITimer timer)
        {
            _container = new SortedSet<TrackedOrder>(new TrackedOrdersContainerComparer());
            _openedOrderCrudAppService = openedOrderCrudAppService ?? throw new ArgumentNullException(nameof(openedOrderCrudAppService));
            _barProvider = barProvider ?? throw new ArgumentNullException(nameof(barProvider));
            _trailingStopCalculator = trailingStopCalculator ?? throw new ArgumentNullException(nameof(trailingStopCalculator));
            _restClient = restClient ?? throw new ArgumentNullException(nameof(restClient));
            _timer = timer ?? throw new ArgumentNullException(nameof(timer));
        }


        /// <summary>
        /// Пересчет Stop-Loss и Take-Profit ордеров
        /// </summary>
        /// <param name="timeFrames"></param>

        //TODO возможно следует переименновать метод, т.к. теперь метод не только выполняет пересчет стоп лосс и тейк профит
        public void ReCalculate(IEnumerable<int> timeFrames)
        {
            var orders = _container.Where(o => timeFrames.Contains(o.OpenedOrder.TimeFrame.Minutes)).ToList();

            orders.ForEach(async o =>
            {
                // Проверка ордера на устаревание
                var check = await CancelOrder(o);

                if (check == false)
                {
                    o.OpenedOrder.StopLoss = await _trailingStopCalculator.CalculateTakeProfitAsync(o.OpenedOrder);
                    //Вызов расчета тейк профита
                }
            });
        }

        public async Task Add(TrackedOrder order)
        {
            //Поменять статус ордеру
            order.OpenedOrder.Order.OrderStatus = OrderStatuses.Tracked;

            //Положим в контейнер
            _container.Add(order);

            //Сохраним в БД
            var orderDto = await _openedOrderCrudAppService.Update(order.OpenedOrder);
        }
        public Task Remove(TrackedOrder order)
        {
            return Task.Run(() =>
            {
                _container.Remove(order);
            });
        }


        public int Count()
        {
            return _container.Count;
        }

        public Task CheckOrders()
        {
            throw new NotImplementedException();
        }

        public IList<TrackedOrder> GetAll()
        {
            return _container.ToList();
        }

        /// <summary>
        /// Проверка ордеров на срабатывание  стоп лоссов и тейк-профитов
        /// </summary>
        /// <returns></returns>
        public async Task<IList<TrackedOrder>> OrderActivationCheck()
        {
            List<TrackedOrder> аctivationOrders = new List<TrackedOrder>();
            //Группируем все ордера сначала по биржам, а потом исключаем повторяющиеся
            var ordersByExchanges = _container
                .GroupBy(o => o.OpenedOrder.Order.ExchangeId);

            foreach (var groupByExchange in ordersByExchanges)
            {
                var exchangeId = groupByExchange.Key;

                var ordersByPair = groupByExchange.GroupBy(o => o.OpenedOrder.Order.PairId);

                foreach (var groupByPair in ordersByPair)
                {
                    var pairId = groupByPair.Key;

                    //Для скачивания нужного бара нужно передать информацию о сделке: название биржи, валютную пару, таймфрейм
                    //нужно каждый ордер сравнить с баром (ценам открытия-закрытия-хайк-лоу)

                    var bar = (await _barProvider.GetList(pairId, exchangeId, TimeSpan.FromMinutes(1), 1))[0];
                    //проверить состояние возвращенного бара

                    foreach (var order in groupByPair)
                    {
                        //Если выявлено превышения значение SL и ТР, то менеджер ордеров должен выставить ордер на выход из позиции на биржу
                        //и дополнительно переместить ордер из TrackedContainer  в PlacedContainer

                        //Проверка Stop Loss и Take Profit
                        if (
                            (order.OpenedOrder.Order.TradeTypes == TradeTypes.Sell
                            && (order.OpenedOrder.StopLoss >= bar.Low || order.OpenedOrder.TakeProfit <= bar.Height))
                            ||
                            (order.OpenedOrder.Order.TradeTypes == TradeTypes.Buy
                            && (order.OpenedOrder.StopLoss <= bar.Height || order.OpenedOrder.TakeProfit >= bar.Low))
                           )
                        {
                            аctivationOrders.Add(order);
                        }
                    }
                }
            }

            return аctivationOrders;
        }

        /// <summary>
        /// Метод отправляет ордера на отмену резерва денежных средств, обновляется статус ордера и удаляются из контейнера.
        /// Отменяются ордера в которых превышено время ожидания в 3 тайм-фрейма
        /// </summary>
        /// <param name="orders"></param>
        private async Task<bool> CancelOrder(TrackedOrder order)
        {
            DateTime dataTimeOrder;
            TimeSpan timeFrame;
            TimeSpan timeDifference;

            DateTime dateTimeNow = _timer.CurrentTime();

            dataTimeOrder = order.OpenedOrder.Order.Date;
            // Получить Тайм фрейм
            timeFrame = order.OpenedOrder.TimeFrame;

            timeDifference = dateTimeNow - dataTimeOrder;

            if (timeDifference >= 3 * timeFrame)
            {
                CancelReserveMoney(order.OpenedOrder, _baseUrl);
                // Изменить статус ордера
                order.OpenedOrder.Order.OrderStatus = OrderStatuses.Canceled;

                //Сохраним в БД
                var orderDto = await _openedOrderCrudAppService.Update(order.OpenedOrder);

                // Удалить из контейнера
                _container.Remove(order);

                return true;
            }

            return false;
        }
        /// <summary>
        /// Отправляет запрос в Аккаунт Менеджер для отмены резерва денежных средств выделенных для ордера
        /// </summary>
        /// <param name="order"></param>
        /// <param name="baseUrl"></param>
        /// <returns></returns>
        private decimal CancelReserveMoney(OpenedOrderDto order, string baseUrl)
        {
            var resource = Path.Combine(baseUrl, "TradingTactic",
                $"CancelReserveMoney?order={order}");

            var request = new RestRequest(resource, Method.GET);

            var response = _restClient.Execute(request);

            var reserveMoneyValue = JsonConvert.DeserializeObject<decimal>(response.Content);

            if (response.IsSuccessful)
            {
                return reserveMoneyValue;
            }
            else
            {
                throw new Exception("Запрос не прошел");
            }
        }
    }

    public class TrackedOrdersContainerComparer : IComparer<TrackedOrder>
    {
        public int Compare(TrackedOrder x, TrackedOrder y)
        {
            if (x.OpenedOrder.TimeFrame < y.OpenedOrder.TimeFrame)
                return -1;

            if (x.OpenedOrder.TimeFrame > y.OpenedOrder.TimeFrame)
                return 1;

            return 0;
        }
    }


}
