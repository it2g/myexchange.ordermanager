﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyExchange.Api.Dto.OpenedOrder;
using MyExchange.OrderManager.Api;
using MyExchange.OrderManager.API.Bl;

namespace MyExchange.OrderManager.Bl
{
    public class TradingTactic : ITradingTacticService
    {
        private readonly IOrderManager _orderManager;

        public TradingTactic(IOrderManager orderManager)
        {
            _orderManager = orderManager;
        }

        public IEnumerable<OpenedOrderDto> GetTradesTradingTactic(Guid tradingTacticContextId)
        {
            var placedOrders = _orderManager.Context.PlacedOrdersContainer.GetAll().Where(o => o.OpenedOrder.TradingTacticContextId == tradingTacticContextId).ToList();

            var trackedOrders = _orderManager.Context.TrackedOrdersContainer.GetAll().Where(o => o.OpenedOrder.TradingTacticContextId == tradingTacticContextId).ToList();

            List<OpenedOrderDto> openedOrdersDto = new List<OpenedOrderDto>();

            foreach (var placedOrder in placedOrders)
            {
                openedOrdersDto.Add(placedOrder.OpenedOrder);
            }

            foreach (var trackedOrder in trackedOrders)
            {
                openedOrdersDto.Add(trackedOrder.OpenedOrder);
            }

            return openedOrdersDto;
        }
    }
}
