﻿using System.Linq;
using System.Threading.Tasks;
using MyExchange.Api.Dto.OpenedOrder;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.TrailingTakeProft_v1_0.Api;

namespace MyExchange.TrailingTakeProft_v1_0
{
    public class TrailingTakeProftV10Calculator : ITrailingTakeProft_v1_0Calculator
    {
        private readonly ITradeCrudAppService _tradeCrudAppService;

        public TrailingTakeProftV10Calculator(ITradeCrudAppService tradeCrudAppService)
        {
            _tradeCrudAppService = tradeCrudAppService;
        }

        public async Task<decimal> CalculateTakeProfitAsync(OpenedOrderDto order)
        {
            //TODO реализовать
            var trade = (await _tradeCrudAppService.GetAll(new TradePagedResultRequestDto
            {
                ExchangeId = order.Order.ExchangeId,
                PairId = order.Order.PairId,
                Sorting = "Date"
            })).Items.Last();

            decimal stopLoss = trade.Price - trade.Price*10/100;

            return order.StopLoss < stopLoss ? stopLoss : (decimal)order.StopLoss;

        }

    }
}
