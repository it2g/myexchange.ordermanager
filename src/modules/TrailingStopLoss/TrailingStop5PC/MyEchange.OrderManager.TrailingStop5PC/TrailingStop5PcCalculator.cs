﻿using MyEchange.TrailingStop5PC.Api;
using System;
using System.Linq;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Bl;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MyExchange.Api.Services.Crud;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto.OpenedOrder;
using MyExchange.Api.Services.Infrastructure;

namespace MyEchange.TrailingStop5PC
{
    public class TrailingStop5PcCalculator : ITrailingStop5PcCalculator
    {
        private readonly ITradeCrudAppService _tradeCrudAppService;
        private readonly ITimer _timer;


        public TrailingStop5PcCalculator([NotNull] ITradeCrudAppService tradeCrudAppService,
            [NotNull] ITimer timer)
        {
            _tradeCrudAppService = tradeCrudAppService ?? throw new ArgumentNullException(nameof(tradeCrudAppService));
            _timer = timer ?? throw new ArgumentNullException(nameof(timer));
        }

        public async Task<decimal> CalculateTakeProfitAsync(OpenedOrderDto order)
        {
            var trade = await _tradeCrudAppService.GetCurrentAsync(_timer.CurrentTime());

            decimal stopLoss = order.StopLoss ??
                               (order.Order.TradeTypes == TradeTypes.Sell
                                   ? order.Order.Price * 0.9M
                                   : order.Order.Price * 1.1M);

            if (order.Order.TradeTypes == TradeTypes.Sell)
            {
                var sl = trade.Price - trade.Price * 10 / 100;

                if (stopLoss < sl)
                {
                    stopLoss = sl;
                }
            }
            else // Bay
            {
                var sl = trade.Price + trade.Price * 10 / 100;

                if (stopLoss > sl)
                {
                    stopLoss = sl;
                }
            }

            return stopLoss;
        }
    }
}
