﻿using Abp.Modules;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using MyExchange.Api.OrderManagment;
using MyExchange.Base;

namespace MyEchange.TrailingStop5PC
{
    [DependsOn(typeof(MyExchangeBaseModule))]
    public class TrailingStop5PCModule : AbpModule
    {
        public bool SkipDbContextRegistration { get; set; }

        /// <summary>
        /// Предварительное подключение конвенций регистрации для контейнера,
        /// для автоматической регистрации контекстов данных, конфигураций и 
        /// компонентов системы (репозиториев и сервисов).
        /// </summary>
        public override void PreInitialize()
        {
            // включение поддержки внедрения коллекции зависимостей.
            // Эта настройка позволяет контейнеру внедрять зависимости в свойства вида T[], IList<T>, IEnumerable<T>, ICollection<T>,
            // пытаясь подставить в них коллекцию со всеми зайденными реализациями типа T. 
            // Если ни одной зависимости типа T нет, контейнер внедрит пустую коллекцию
            IocManager.IocContainer.Kernel.Resolver.AddSubResolver(new CollectionResolver(IocManager.IocContainer.Kernel, true));
            IocManager.Register<ITrailingStopCalculator, TrailingStop5PcCalculator>(Abp.Dependency.DependencyLifeStyle
                .Transient);
        }

        /// <summary>
        /// Регистрация компонентов текущей сборки и сборки модели справочников в контейнере
        /// </summary>
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TrailingStop5PCModule).Assembly);
        }

        /// <summary>
        /// Выполнение настроечной логики после завершения сборки контейнера
        /// </summary>
        public override void PostInitialize()
        {
        }


    }
}
