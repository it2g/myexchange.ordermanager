﻿using System;
using System.Collections.Generic;
using System.Text;
using MyExchange.Api.Dto.OpenedOrder;
using MyExchange.OrderManager.API.Bl;
using MyExchange.Web.Controllers;

namespace MyExchange.OrderManager.Web.Controllers
{
    public class TradingTacticController : MyExchangeControllerBase<ITradingTacticService>
    {
        public TradingTacticController(ITradingTacticService service) : base(service)
        {

        }

        public IEnumerable<OpenedOrderDto> GetTradesTradingTactic(Guid tradingTacticContextId)
        {
            if (tradingTacticContextId != default)
                return Service.GetTradesTradingTactic(tradingTacticContextId);
            else
                return null;
        }
    }
}
