﻿using Abp.Dependency;
using Abp.Modules;
using Abp.Reflection.Extensions;
using RestSharp;

namespace MyExchange.OrderManager.Web
{
  
    public class OrderManagerWebModule : AbpModule
    {
        public override void PreInitialize()
        {

        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(OrderManagerWebModule).GetAssembly());
           
        }

        public override void PostInitialize()
        {

        }
    }
}
