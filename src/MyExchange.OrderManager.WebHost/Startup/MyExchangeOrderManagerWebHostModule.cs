﻿using Abp.Hangfire;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.RemoteEventBus.Kafka;
using MyExchange.OrderManager.Web;
using MyExchange.OrderManager;

namespace MyExchange.OrderManager.WebHost.Startup
{
    [DependsOn(typeof(AbpHangfireAspNetCoreModule),
               typeof(AbpRemoteEventBusKafkaModule),
               typeof(OrderManagerModule),
               typeof(OrderManagerWebModule)
               )]
    public class MyExchangeOrderManagerWebHostModule : AbpModule
    {
        public override void PreInitialize()
        {
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MyExchangeOrderManagerWebHostModule).GetAssembly());
        }

        public override void PostInitialize()
        {
        }
    }
}
