﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Services;
using System;
using System.Threading.Tasks;
using Abp.AutoMapper;
using MyExchange.Api.Dto.TradeExpertAdvice;
using MyExchange.Services.Crud;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class TradeExpertAdviceController : MyExchangeControllerBase<TradeExpertAdviceCrudAppService>
    {
        public TradeExpertAdviceController(TradeExpertAdviceCrudAppService crudService) : base(crudService)
        {
        }

        [HttpGet]
        public async Task<PagedResultDto<TradeExpertAdviceDto>> Get()
        {

            return await Service.GetAll(new TradeExpertAdvicePagedResultRequestDto());

        }

        [HttpGet("{id}")]
        public async Task<TradeExpertAdviceDto> Get([FromRoute]Guid id)
        {
            return await Service.Get(id);
        }

        [HttpPost]
        public async Task<TradeExpertAdviceDto> Post([FromBody]CreateTradeExpertAdviceDto input)
        {
            var dto = await Service.Create(input.MapTo<TradeExpertAdviceDto>());

            return dto;
        }

        [HttpPut("{id}")]
        public async Task<TradeExpertAdviceDto> Put([FromRoute] Guid id, [FromBody]  TradeExpertAdviceDto input)
        {

            var dto = await Service.Get(id);

            dto = await Service.Update(ObjectMapper.Map(input, dto));

            return dto;
        }

        [HttpDelete("{id}")]
        public async Task Delete([FromRoute] Guid id)
        {
            await Service.Delete(id);
        }
    }
}
