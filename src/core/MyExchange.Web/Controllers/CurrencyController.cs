﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Extensions;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Services.Crud;
using MyExchange.BackgroundJobs;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class CurrencyController : MyExchangeControllerBase<ICurrencyCrudAppService>
    {
        private IBackgroundJobManager _backgroundJobManager;

        public CurrencyController(ICurrencyCrudAppService service, 
            [NotNull] IBackgroundJobManager backgroundJobManager) : base(service)
        {
            _backgroundJobManager = backgroundJobManager ?? throw new ArgumentNullException(nameof(backgroundJobManager));
        }

        [HttpGet]
        public async Task<PagedResultDto<CurrencyDto>> Get()
        {

            return await Service.GetAll(new CurrencyPagedResultRequestDto()
            {
                MaxResultCount = 1000
            });

        }

        [HttpGet("{id}")]
        public async Task<CurrencyDto> Get([FromRoute]Guid id)
        {
            return await Service.Get(id);
        }

        [HttpPost]
        public async Task<CurrencyDto> Post(CreateCurrencyDto input)
        {
            var dto = await Service.Create(input.MapTo<CurrencyDto>());

            return dto;
        }

        [HttpPut("{id}")]
        public async Task<CurrencyDto> Put([FromRoute] Guid id, [FromBody] CurrencyDto input)
        {

            var dto = await Service.Get(id);

            dto = await Service.Update(ObjectMapper.Map(input, dto));

            return dto;
        }

        [HttpDelete("{id}")]
        public async Task Delete([FromRoute] Guid id)
        {
            await Service.Delete(id);

        }

        /// <summary>
        /// Загрузка валют с биржи в БД
        /// </summary>
        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> LoadFromExchange([FromBody] string exchangeId )
        {
            if (exchangeId.IsNullOrEmpty()) return NotFound(new Exception("Ошибочный идентификатор биржи")) ;

            await _backgroundJobManager.EnqueueAsync<ReadCurrenciesJob, string>(exchangeId);

            return Ok();
        }

    }
}
