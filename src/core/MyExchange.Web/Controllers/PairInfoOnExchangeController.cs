﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Services;
using System;
using System.Threading.Tasks;
using Abp.AutoMapper;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Services.Crud;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class PairInfoOnExchangeController : MyExchangeControllerBase<PairInfoOnExchangeCrudAppService>
    {
        public PairInfoOnExchangeController(PairInfoOnExchangeCrudAppService crudService) : base(crudService)
        {

        }

        [HttpGet]
        public async Task<PagedResultDto<PairInfoOnExchangeDto>> Get()
        {

            return await Service.GetAll(new PairInfoOnExchangePagedResultRequestDto());

        }

        [HttpGet("{id}")]
        public async Task<PairInfoOnExchangeDto> Get([FromRoute]Guid id)
        {
            return await Service.Get(id);
        }

        [HttpPost]
        public async Task<PairInfoOnExchangeDto> Post([FromBody] CreatePairInfoOnExchangeDto input)
        {
            var dto = await Service.Create(input.MapTo<PairInfoOnExchangeDto>());

            return dto;
        }

        [HttpPut("{id}")]
        public async Task<PairInfoOnExchangeDto> Put([FromRoute] Guid id,[FromBody]  PairInfoOnExchangeDto input)
        {

            var dto = await Service.Get(id);

            dto = await Service.Update(ObjectMapper.Map(input, dto));

            return dto;
        }

        [HttpDelete("{id}")]
        public async Task Delete([FromRoute] Guid id)
        {
            await Service.Delete(id);
        }
    }
}
