﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using MyExchange.Api.Dto.CurrencyUserBalance;
using MyExchange.Api.Services.Crud;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class CurrencyUserBalanceController : MyExchangeControllerBase<ICurrencyUserBalanceCrudAppService>
    {
        public CurrencyUserBalanceController(ICurrencyUserBalanceCrudAppService service) : base(service)
        {
        }

        [HttpGet()]
        public async Task<PagedResultDto<CurrencyUserBalanceDto>> Get([FromRoute]CurrencyUserBalancePagedResultRequestDto input)
        {
            return await Service.GetAll(input);
        }

        [HttpGet("{id}")]
        public async Task<CurrencyUserBalanceDto> Get([FromRoute]Guid id)
        {
            return await Service.Get(id);
        }
       
    }
}
