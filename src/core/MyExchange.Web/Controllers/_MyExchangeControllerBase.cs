﻿using Abp.Application.Services;
using MyExchange.Controllers;
using MyExchange.Services;
using System;
using MyExchange.Services.Crud;

namespace MyExchange.Web.Controllers
{
    public class MyExchangeControllerBase<TService> : MyExchangeControllerBase
         where TService : class, IApplicationService
    {
        protected readonly TService Service;
        private CurrencyCrudAppService service;

        public MyExchangeControllerBase(TService crudService)
        {
            Service = crudService ?? throw new ArgumentNullException(nameof(crudService));
        }

        public MyExchangeControllerBase(CurrencyCrudAppService service)
        {
            this.service = service;
        }
    }
}
