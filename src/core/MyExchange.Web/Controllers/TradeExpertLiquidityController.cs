﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Services;
using System;
using System.Threading.Tasks;
using Abp.AutoMapper;
using MyExchange.Api.Dto.TradeExpertLiquidity;
using MyExchange.Services.Crud;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class TradeExpertLiquidityController : MyExchangeControllerBase<TradeExpertLiquidityCrudAppService>
    {
        public TradeExpertLiquidityController(TradeExpertLiquidityCrudAppService crudService) : base(crudService)
        {
        }

        [HttpGet]
        public async Task<PagedResultDto<TradeExpertLiquidityDto>> Get()
        {

            return await Service.GetAll(new TradeExpertLiquidityPagedResultRequestDto());

        }

        [HttpGet("{id}")]
        public async Task<TradeExpertLiquidityDto> Get([FromRoute]Guid id)
        {
            return await Service.Get(id);
        }

        [HttpPost]
        public async Task<TradeExpertLiquidityDto> Post([FromBody]CreateTradeExpertLiquidityDto input)
        {
            var dto = await Service.Create(input.MapTo<TradeExpertLiquidityDto>());

            return dto;
        }

        [HttpPut("{id}")]
        public async Task<TradeExpertLiquidityDto> Put([FromRoute] Guid id,  [FromBody]TradeExpertLiquidityDto input)
        {

            var dto = await Service.Get(id);

            dto = await Service.Update(ObjectMapper.Map(input, dto));

            return dto;
        }

        [HttpDelete("{id}")]
        public async Task Delete([FromRoute] Guid id)
        {
            await Service.Delete(id);
        }
    }
}
