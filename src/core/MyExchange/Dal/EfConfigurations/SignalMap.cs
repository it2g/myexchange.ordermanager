﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.EfConfigurations
{
    public class SignalMap : BaseEntityMap<Signal>
    {
        public override void Configure(EntityTypeBuilder<Signal> builder)
        {
            base.Configure(builder);
            builder.Property(u => u.ExchangeId).HasColumnName("exchange_id").IsRequired();
            builder.Property(u => u.PairId).HasColumnName("pair_id").IsRequired();
            builder.HasOne(u => u.Pair).WithMany().HasForeignKey(x => x.PairId).IsRequired();
            builder.Property(u => u.IndicatorId).HasColumnName("indicator_id").IsRequired();
            builder.Property(u => u.IndicatorContextId).HasColumnName("indicator_context_id").IsRequired();
            builder.Property(u => u.Price).HasColumnName("price").IsRequired();
            builder.Property(u => u.SignalTime).HasColumnName("signal_time").IsRequired();
            builder.Property(u => u.Stoploss).HasColumnName("stoploss").IsRequired();
            builder.Property(u => u.TakeProfit).HasColumnName("take_profit").IsRequired();
            builder.Property(u => u.Trend).HasColumnName("trend").IsRequired();
            builder.Property(u => u.TimeFrame).HasColumnName("time_frame").IsRequired();

            builder.ToTable("signals", "core");
        }
    }
}
