﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.EfConfigurations
{
    public class CurrencyMap : BaseEntityMap<Currency>
    {
        public override void Configure(EntityTypeBuilder<Currency> builder)
        {
            base.Configure(builder);
            builder.Property(u => u.BrifName).HasColumnName("brif_name").IsRequired();
            builder.Property(u => u.FullName).IsRequired(false).HasColumnName("full_name");
            

            builder.ToTable("currencies", "core");
        }
    }
}
