﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.EfConfigurations
{
    public class TradeExpertLiquidityMap : BaseEntityMap<TradeExpertLiquidity>
    {
        public override void Configure(EntityTypeBuilder<TradeExpertLiquidity> builder)
        {
            base.Configure(builder);
            builder.Property(u => u.Liquidity).HasColumnName("liquidity").IsRequired();
            builder.Property(u => u.Date).HasColumnName("date").IsRequired();
            builder.Property(u => u.TradeExpertId).HasColumnName("trade_expert_id").IsRequired();

            builder.ToTable("trade_expert_liquidities", "core");
        }
    }
}
