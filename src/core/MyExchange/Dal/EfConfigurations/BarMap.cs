﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.Api.Data;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.EfConfigurations
{
    public class BarMap : BaseEntityMap<Bar>
    {
        public override void Configure(EntityTypeBuilder<Bar> builder)
        {
            base.Configure(builder);
            builder.Property(x => x.BarTime).HasColumnName("bar_time");
            builder.Property(x => x.PairId).HasColumnName("pair_id");
            builder.Property(x => x.ExchangeId).HasColumnName("exchange_id").IsRequired();
            builder.Property(x => x.Height).HasColumnName("height");
            builder.Property(x => x.Open).HasColumnName("open");
            builder.Property(x => x.Low).HasColumnName("low");
            builder.Property(x => x.Value).HasColumnName("value");
            builder.Property(x => x.Close).HasColumnName("close");
            builder.Property(x => x.TimeFrame).HasColumnName("time_frame");

            builder.ToTable("bars", "core");
        }
    }
}
