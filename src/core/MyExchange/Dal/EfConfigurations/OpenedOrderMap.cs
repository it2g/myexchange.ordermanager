﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.EfConfigurations
{
    public class OpenedOrderMap : BaseEntityMap<OpenedOrder>
    {
        public override void Configure(EntityTypeBuilder<OpenedOrder> builder)
        {
            base.Configure(builder);
            builder.Property(u => u.UserId).HasColumnName("user_id").IsRequired();
            builder.Property(u => u.Comment).IsRequired(false).HasColumnName("comment");
            builder.Property(u => u.StopLoss).IsRequired(false).HasColumnName("stop_loss");
            builder.Property(u => u.TakeProfit).IsRequired(false).HasColumnName("take_profit");
            builder.Property(u => u.OrderId).IsRequired().HasColumnName("order_id");
            builder.Property(u => u.TradingTacticContextId).IsRequired().HasColumnName("trading_tactic_context_id");
            builder.Property(u => u.TimeFrame).IsRequired().HasColumnName("time_frame");
            builder.Property(x => x.IsDeleted).HasColumnName("is_deleted");

            builder.HasOne(u => u.Order).WithOne().IsRequired()
                                    .HasForeignKey<OpenedOrder>(order => order.OrderId );

            builder.ToTable("opened_orders", "core");
        }
    }
}
