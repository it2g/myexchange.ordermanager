﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.EfConfigurations
{
    public class PairMap : BaseEntityMap<Pair>
    {
        public override void Configure(EntityTypeBuilder<Pair> builder)
        {
            base.Configure(builder);
            builder.Property(x => x.IsDeleted).HasColumnName("id_deleted");
            
            builder.Property(u => u.Currency1Id).HasColumnName("currency1_id").IsRequired();
            builder.HasOne(x => x.Currency1).WithMany().HasForeignKey(x => x.Currency1Id).IsRequired();

            builder.Property(u => u.Currency2Id).IsRequired().HasColumnName("currency2_id");
            builder.HasOne(x => x.Currency2).WithMany().HasForeignKey(x => x.Currency2Id).IsRequired();

            builder.ToTable("pairs", "core");
        }
    }
}
