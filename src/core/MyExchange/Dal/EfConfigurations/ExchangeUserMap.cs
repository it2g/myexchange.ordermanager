﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.EfConfigurations
{
    public class ExchangeUserMap : BaseEntityMap<ExchangeUser>
    {
        public override void Configure(EntityTypeBuilder<ExchangeUser> builder)
        {
            base.Configure(builder);
            builder.Property(u => u.UserId).HasColumnName("user_id").IsRequired();
            builder.Ignore(x => x.User);
            builder.Property(u => u.ApiOpenKey).IsRequired(false).HasColumnName("api_open_key");
            builder.Property(u => u.ApiSecretKey).IsRequired(false).HasColumnName("api_secret_key");
            builder.Property(u => u.Salt).IsRequired(false).HasColumnName("salt");
            builder.Property(u => u.ExternalId).HasColumnName("external_id").IsRequired();
            builder.Property(u => u.ExchangeId).HasColumnName("exchange_id").IsRequired();
            
            builder.HasOne(x => x.User).WithMany().HasForeignKey(x => x.UserId).IsRequired();

            builder.ToTable("exchange_user", "core");
        }
    }
}
