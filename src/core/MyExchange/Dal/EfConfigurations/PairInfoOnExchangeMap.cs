﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.EfConfigurations
{
    public class PairInfoOnExchangeMap : BaseEntityMap<PairInfoOnExchange>
    {
        public override void Configure(EntityTypeBuilder<PairInfoOnExchange> builder)
        {
            base.Configure(builder);
            builder.Property(u => u.Avarage).HasColumnName("avarage").IsRequired();
            builder.Property(u => u.BuyPrice).HasColumnName("buy_price").IsRequired();
            builder.Property(u => u.ExchangeId).HasColumnName("exchange_id").IsRequired();
            builder.Property(u => u.High).HasColumnName("higt").IsRequired();
            builder.Property(u => u.LastTrade).HasColumnName("last_trade").IsRequired();
            builder.Property(u => u.Low).HasColumnName("low").IsRequired();
            builder.Property(u => u.SellPrice).HasColumnName("sell_price").IsRequired();
            builder.Property(u => u.Updated).HasColumnName("updated").IsRequired();
            builder.Property(u => u.Value).HasColumnName("value").IsRequired();
            builder.Property(u => u.ValueCurr).HasColumnName("value_curr").IsRequired();

            builder.Property(u => u.PairId).IsRequired().HasColumnName("pair_id");
            builder.HasOne(x => x.Pair).WithMany().HasForeignKey(x => x.PairId).IsRequired();

            builder.ToTable("pair_info_on_exchange", "core");
        }
    }
}
