﻿using System;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using MyExchange.Api.Dal;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.Repositories
{

    public class OpenedOrderRepository : EfCoreRepositoryBase<MyExchangeDbContext, OpenedOrder, Guid>, IOpenedOrderRepository
    {
        public OpenedOrderRepository(IDbContextProvider<MyExchangeDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
