﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using MyExchange.Api.Dal;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.Repositories
{
   public class CurrencyUserBalanceRepository : EfCoreRepositoryBase<MyExchangeDbContext, CurrencyUserBalance, Guid>, ICurrencyUserBalanceRepository
    {
        public CurrencyUserBalanceRepository(IDbContextProvider<MyExchangeDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
