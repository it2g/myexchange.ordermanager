﻿using System;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using MyExchange.Api.Dal;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.Repositories
{
    public class TradeRepository : EfCoreRepositoryBase<MyExchangeDbContext, Trade, Guid>, ITradeRepository
    {
        public TradeRepository(IDbContextProvider<MyExchangeDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public override Trade Insert(Trade entity)
        {
            return base.Insert(entity);
        }
    }
}
