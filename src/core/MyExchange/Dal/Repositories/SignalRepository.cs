﻿using System;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using MyExchange.Api.Dal;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.Repositories
{
    public class SignalRepository : EfCoreRepositoryBase<MyExchangeDbContext, Signal, Guid>, ISignalRepository
    {
        public SignalRepository(IDbContextProvider<MyExchangeDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
