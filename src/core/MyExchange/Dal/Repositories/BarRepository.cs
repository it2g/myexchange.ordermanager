﻿using System;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using MyExchange.Api.Dal;
using MyExchange.Api.Data;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.Repositories
{
    public class BarRepository : EfCoreRepositoryBase<MyExchangeDbContext, Bar, Guid>, IBarRepository
    {
        public BarRepository(IDbContextProvider<MyExchangeDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
