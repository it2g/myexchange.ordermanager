﻿using System;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using MyExchange.Api.Dal;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.Repositories
{

    public class CurrencyRepository : EfCoreRepositoryBase<MyExchangeDbContext, Currency, Guid>, ICurrencyRepository
    {
        public CurrencyRepository(IDbContextProvider<MyExchangeDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
