﻿using System;
using Abp.AutoMapper;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using MyExchange.Api.Services.Infrastructure;
using MyExchange.Base;
using MyExchange.Web;
using MyExchange.Dal;
using MyExchange.EntityFrameworkCore;
using MyExchange.Services.Infrastructure;
using MyExchange.Configuration;
using MyExchange.Api.Services.Bl;
using MyExchange.Services.Bl;

namespace MyExchange
{
    [DependsOn(typeof(MyExchangeBaseModule),
        typeof(MyExchangeEntityFrameworkModule),
        typeof(AbpAutoMapperModule)
        )]

    public class MyExchangeMainModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public MyExchangeMainModule(IHostingEnvironment env)
        {
            _appConfiguration = env.GetAppConfiguration();
        }
        /* Used it tests to skip dbcontext registration, in order to use in-memory database of EF Core */
        public bool SkipDbContextRegistration { get; set; }

        /// <summary>
        /// Предварительное подключение конвенций регистрации для контейнера,
        /// для автоматической регистрации контекстов данных, конфигураций и 
        /// компонентов системы (репозиториев и сервисов).
        /// </summary>
        public override void PreInitialize()
        {
            //IocManager.AddConventionalRegistrar(new ConfigurationConventionalRegistrar());
            //IocManager.AddConventionalRegistrar(new ComponentConventionalRegistrar());
            //Регистрация контекста БД в модуле
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<MyExchangeDbContext>(options =>
                {
                    if (options.ExistingConnection != null)
                    {
                        MyExchangeDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    }
                    else
                    {
                        MyExchangeDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    }
                });
            }

            //Настройка конфигурации Automapper
            Configuration.Modules.AbpAutoMapper().Configurators.Add(config =>
                config.AddProfiles(typeof(MyExchangeMainModule).Assembly));

            // включение поддержки внедрения коллекции зависимостей.
            // Эта настройка позволяет контейнеру внедрять зависимости в свойства вида T[], IList<T>, IEnumerable<T>, ICollection<T>,
            // пытаясь подставить в них коллекцию со всеми зайденными реализациями типа T. 
            // Если ни одной зависимости типа T нет, контейнер внедрит пустую коллекцию
            IocManager.IocContainer.Kernel.Resolver.AddSubResolver(new CollectionResolver(IocManager.IocContainer.Kernel, true));
        }

        /// <summary>
        /// Регистрация компонентов текущей сборки и сборки модели справочников в контейнере
        /// </summary>
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MyExchangeMainModule).Assembly);

            // регистрация делегата-фабрики компонентов для возможности другим конпонентам
            // опосредованно запрашивать зависимости из контейнера
            IocManager.IocContainer.Register(
                Component.For<Func<Type, string, object>>().Instance((type, name) => IocManager.Resolve(type)));
            
            IocManager.IocContainer.Register(Component.For<ITimer>()
                .Instance(new Timer((options) =>
                {
                    options.NeedRelativeTime = _appConfiguration.GetValue<bool>("Timer:NeedRelativeTime");
                    
                    if (options.NeedRelativeTime)
                    {
                        options.RelativeTimeReference =
                            _appConfiguration.GetValue<DateTime>("Timer:RelativeTimeReference");

                        options.Scale = _appConfiguration.GetValue<uint>("Timer:Scale");
                    }
                }))
                .LifestyleSingleton()
            );

            IocManager.Register<IBarProvider, BarFromDbProvider>();
        }

        /// <summary>
        /// Выполнение настроечной логики после завершения сборки контейнера
        /// </summary>
        public override void PostInitialize()
        {
        }


    }
}
