﻿using System;
using System.Collections.Generic;
using System.Text;
using JetBrains.Annotations;
using MyExchange.Api.Services.Infrastructure;

namespace MyExchange.Services.Infrastructure
{
    /// <summary>
    /// Позволяет установить масштаб и точку отсчета времени 
    /// </summary>
    public class Timer : ITimer
    {
        /// <summary>
        /// Нужна ли трансформация времени
        /// </summary>
        private bool _needRelativeTime;

        /// <summary>
        /// Относительная точка отсчета
        /// </summary>
        private DateTime _relativeTimeReference;

        /// <summary>
        /// Разница между текушим временем и заданной точкой отсчета
        /// в момент инициализации и первого использования таймера
        /// </summary>
        private TimeSpan _delta;

        /// <summary>
        /// Масштабный коэф-т времени
        /// </summary>
        private uint _scale;

        public Timer(Action<TimerOptions> configure)
        {
            var options = new TimerOptions()
            {
                //По умолчанию таймер не масштабирует время и не смещает
                //точку отсчета времени
                NeedRelativeTime = false,
            };

            configure(options);

            _needRelativeTime = options.NeedRelativeTime;

            //Если после конфигурирования настроек требуется изменить
            //масштаб и точку отсчета
            if (options.NeedRelativeTime)
            {
                _relativeTimeReference = options.RelativeTimeReference;

                _scale = options.Scale;

                _delta = DateTime.UtcNow - _relativeTimeReference;
            }
            else
            {
                _scale = 1;

                _delta = TimeSpan.Zero;
            }
        }

        /// <summary>
        /// Вычисляется дата и время с учетом смешения координаты текущейго времени
        /// и используемого масштабного коэф-та времени
        /// </summary>
        public DateTime CurrentTime()
        {
            if (_needRelativeTime)
            {
                return _relativeTimeReference + ((DateTime.UtcNow - _delta) - _relativeTimeReference) * _scale;
               
            }
            return DateTime.UtcNow;
        }

        public TimeSpan SpeedUpInMilliSeconds(int millisecond)
        {
            return SpeedUp(TimeSpan.FromMilliseconds(millisecond));
        }

        public TimeSpan SpeedUpInSeconds(double second)
        {
            return SpeedUp(TimeSpan.FromSeconds(second));
        }

        public TimeSpan SpeedUpInMinutes(double minutes)
        {
            return SpeedUp(TimeSpan.FromMinutes(minutes));
        }

        public TimeSpan SpeedUpInHоurs(int hours)
        {
            return SpeedUp(TimeSpan.FromHours(hours));
        }
        /// <summary>
        /// Время взятое из истории фида, масштабируется в сторону ускорения времени. 
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public TimeSpan SpeedUp(TimeSpan time)
        {
            return _needRelativeTime? time / _scale : time;
        }

        /// <summary>
        /// Реальное время масштабируется в сторону замедления
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public TimeSpan SlowDoun(TimeSpan time)
        {
            return _needRelativeTime ? time *_scale : time; ;
        }
    }
}
