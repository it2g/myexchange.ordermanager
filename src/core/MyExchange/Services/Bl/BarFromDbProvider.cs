﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using JetBrains.Annotations;
using MyExchange.Api.Constants;
using MyExchange.Api.Data;
using MyExchange.Api.Dto.Bar;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;
using MyExchange.Api.Services.Infrastructure;


namespace MyExchange.Services.Bl
{
    /// <summary>
    /// Предоставляет сервис управления свечами
    /// </summary>
    public class BarFromDbProvider : IBarProvider
    {
        private IBarCrudAppService _barCrudAppService;
        private ITimer _timer;

        public BarFromDbProvider([NotNull] IBarCrudAppService barCrudAppService,
            [NotNull] ITimer timer)
        {
            _barCrudAppService = barCrudAppService ?? throw new ArgumentNullException(nameof(barCrudAppService));
            _timer = timer ?? throw new ArgumentNullException(nameof(timer));
        }

        public async Task<IList<BarDto>>  GetList(Guid pairId, string exchangeId, TimeSpan timeFrame, DateTime begin, DateTime end, int countBars = 0)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pairId"></param>
        /// <param name="exchangeId"></param>
        /// <param name="timeFrame"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public async Task<IList<BarDto>> GetList(Guid pairId, string exchangeId, TimeSpan timeFrame, DateTime begin, DateTime end)
        {
            throw new NotImplementedException();

        }

        /// <summary>
        /// TODO нужен модульный тест для этого метода
        /// </summary>
        public async Task<IList<BarDto>> GetList(Guid pairId, string exchangeId, TimeSpan timeFrame, int countBars = 0)
        {
            var response = await _barCrudAppService.GetAll(new BarPagedResultRequestDto
            {
                ExchangeId = exchangeId,
                PairId = pairId,
                TimeFrame = timeFrame,
                CurrentTime = _timer.CurrentTime(),
                CountBars = countBars
            });

            return response.Items.ToList();
        }
    }
}
