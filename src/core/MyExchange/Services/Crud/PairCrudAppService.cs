﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.AutoMapper;
using JetBrains.Annotations;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services.Crud
{
    public class PairCrudAppService : MyExchangeCrudServiceBase<Pair, PairDto, PairPagedResultRequestDto, Guid>, IPairCrudAppService
    {
        private readonly ICurrencyRepository _currencyRepository;
        public PairCrudAppService(IPairRepository repository,
            [NotNull] ICurrencyRepository currencyRepository) : base(repository)
        {
            _currencyRepository = currencyRepository ?? throw new ArgumentNullException(nameof(currencyRepository));
        }

        public override async Task<PairDto> Create(PairDto input)
        {
            var dto = await base.Create(input);

            dto.Currency1 = (await _currencyRepository.GetAsync(dto.Currency1.Id)).MapTo<CurrencyDto>();
            dto.Currency2 = (await _currencyRepository.GetAsync(dto.Currency2.Id)).MapTo<CurrencyDto>();
            dto.Name = $"{dto.Currency1.BrifName}/{dto.Currency2.BrifName}";
            return dto;
        }

        public async Task<Guid> GetPairId(string currency1Name, string currency2Name)
        {
            return (await Repository.FirstOrDefaultAsync(x =>
                x.Currency1.BrifName.ToUpper().Equals(currency1Name.ToUpper()) &&
                x.Currency2.BrifName.ToUpper().Equals(currency2Name.ToUpper()))).Id;
        }
        protected override IQueryable<Pair> CreateFilteredQuery(PairPagedResultRequestDto input)
        {
            var query =  base.CreateFilteredQuery(input);

            if (input.PairSettings != null)
            {
                var pair = input.PairSettings;
                query = query.Where(x =>
                    string.Equals(x.Currency1.BrifName.ToUpper(), pair.Pair.Currency1.BrifName.ToUpper())
                    && String.Equals(x.Currency2.BrifName.ToUpper(), pair.Pair.Currency2.BrifName.ToUpper())
                    );
            }

            return query;
        }
    }
}
