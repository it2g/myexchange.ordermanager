﻿using System;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.TradeExpertAdvice;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services.Crud
{
    public class TradeExpertAdviceCrudAppService : MyExchangeCrudServiceBase<TradeExpertAdvice, TradeExpertAdviceDto, TradeExpertAdvicePagedResultRequestDto, Guid>, ITradeExpertAdviceCrudAppService
    {
        public TradeExpertAdviceCrudAppService(ITradeExpertAdviceRepository repository) : base(repository)
        {
        }

    }
}
