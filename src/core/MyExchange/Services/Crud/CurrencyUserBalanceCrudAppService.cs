﻿using System;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.CurrencyUserBalance;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services.Crud
{
    public class CurrencyUserBalanceCrudAppService : MyExchangeCrudServiceBase<CurrencyUserBalance, CurrencyUserBalanceDto, CurrencyUserBalancePagedResultRequestDto, Guid>, ICurrencyUserBalanceCrudAppService
    {
        public CurrencyUserBalanceCrudAppService(ICurrencyUserBalanceRepository repository) : base(repository)
        {
        }
    }
}
