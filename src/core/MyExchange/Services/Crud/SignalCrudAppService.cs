﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.Signal;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services.Crud
{
    public class SignalCrudAppService : MyExchangeCrudServiceBase<Signal, SignalDto, SignalPagedResultRequestDto, Guid>, ISignalCrudAppService
    {
        public SignalCrudAppService(ISignalRepository repository) : base(repository)
        {
        }

       

        protected override IQueryable<Signal> CreateFilteredQuery(SignalPagedResultRequestDto input)
        {
            var query = base.CreateFilteredQuery(input);

            return query;
        }
    }
}
