﻿using System;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services.Crud
{
    public class CurrencyCrudAppService : MyExchangeCrudServiceBase<Currency, CurrencyDto, CurrencyPagedResultRequestDto,  Guid>, ICurrencyCrudAppService
    {
        public CurrencyCrudAppService(ICurrencyRepository repository) : base(repository)
        {

        }

       

        //public virtual async Task<PagedResultDto<TEntityDto>> GetAll(TGetAllInput input)
        //{
        //    CheckGetAllPermission();

        //    var query = CreateFilteredQuery(input);

        //    var totalCount = await AsyncQueryableExecuter.CountAsync(query);

        //    query = ApplySorting(query, input);
        //    query = ApplyPaging(query, input);

        //    var entities = await AsyncQueryableExecuter.ToListAsync(query);

        //    return new PagedResultDto<TEntityDto>(
        //        totalCount,
        //        entities.Select(MapToEntityDto).ToList()
        //    );
        //}
        //protected virtual IQueryable<TEntity> ApplySorting(IQueryable<TEntity> query, TGetAllInput input)
        //{
        //    //Try to sort query if available
        //    var sortInput = input as ISortedResultRequest;
        //    if (sortInput != null)
        //    {
        //        if (!sortInput.Sorting.IsNullOrWhiteSpace())
        //        {
        //            return query.OrderBy(sortInput.Sorting);
        //        }
        //    }

        //    //IQueryable.Task requires sorting, so we should sort if Take will be used.
        //    if (input is ILimitedResultRequest)
        //    {
        //        return query.OrderByDescending(e => e.Id);
        //    }

        //    //No sorting
        //    return query;
        //}

    }
}
