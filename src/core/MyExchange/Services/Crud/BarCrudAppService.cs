﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.AutoMapper;
using JetBrains.Annotations;
using MyExchange.Api.Dal;
using MyExchange.Api.Data;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.Bar;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services.Crud
{
    public class BarCrudAppService : MyExchangeCrudServiceBase<Bar, BarDto, BarPagedResultRequestDto, Guid>, IBarCrudAppService
    {
        public BarCrudAppService(IBarRepository repository) : base(repository)
        {}

        protected override IQueryable<Bar> CreateFilteredQuery(BarPagedResultRequestDto input)
        {
            var query =  base.CreateFilteredQuery(input);

            if (input.PairId != null && input.PairId != Guid.Empty)
            {
                query = query.Where(x => x.PairId == input.PairId );
            }
            if (!string.IsNullOrEmpty(input.ExchangeId))
            {
                query = query.Where(x => x.ExchangeId == input.ExchangeId);
            }
            if (input.TimeFrame != null && input.TimeFrame !=TimeSpan.Zero)
            {
                query = query.Where(x => x.TimeFrame == input.TimeFrame);
            }
            if (input.CurrentTime != null )
            {
                query = query.Where(x => x.BarTime <= input.CurrentTime);
            }
            if (input.CountBars != null )
            {
                query = query.OrderByDescending(s => s.BarTime)
                    .Take((int)input.CountBars)
                    .OrderByDescending(s => s.BarTime);
            }

            return query;
        }
    }
}
