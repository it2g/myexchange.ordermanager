﻿using System;
using System.Linq;
using Castle.Core.Internal;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services.Crud
{
    public class PairInfoOnExchangeCrudAppService :
        MyExchangeCrudServiceBase<PairInfoOnExchange, PairInfoOnExchangeDto, PairInfoOnExchangePagedResultRequestDto, Guid>, IPairInfoOnExchangeCrudAppService
    {
        public PairInfoOnExchangeCrudAppService(IPairInfoOnExchangeRepository repository) : base(repository)
        {
        }

        protected override IQueryable<PairInfoOnExchange> CreateFilteredQuery(PairInfoOnExchangePagedResultRequestDto input)
        {
            var query = base.CreateFilteredQuery(input);

            if (!input.ExchangeId.IsNullOrEmpty())
            {
                query = query.Where(pair => pair.ExchangeId == input.ExchangeId);
            }

            return query;
        }
    }
}
