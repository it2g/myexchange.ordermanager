﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MyExchange.Api.Data;
using MyExchange.Api.Exchange;
using MyExchange.Api.Services.Crud;

namespace MyExchange.Exchange
{
    public class ExchangeUserKeyProvider : ApplicationService, IExchangeUserKeyProvider
    {
        private IExchangeUserCrudAppService _exchangeUserCrudAppService;

        public ExchangeUserKeyProvider(IExchangeUserCrudAppService exchangeUserCrudAppService)
        {
            _exchangeUserCrudAppService = exchangeUserCrudAppService;
        }

        public async Task<ExchangeUserKeyPair> GetKeyAsync(string exchangeId)
        {                      
            var userId = (long)AbpSession.UserId;

            var exchangeUser = await  _exchangeUserCrudAppService.Get(exchangeId, userId);

            var keyPair = new ExchangeUserKeyPair
            {
                ExchangeUserId = exchangeUser.Id,
                ApiOpenKey = exchangeUser.ApiOpenKey,
                ApiSecretKey = exchangeUser.ApiSecretKey
            };
            
            return keyPair;
        }
    }
}
