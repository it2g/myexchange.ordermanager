﻿using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Dependency;
using MyExchange.Api.Exchange;

namespace MyExchange.Exchange
{
    public class ExchangeProvider : ApplicationService, IExchangeProvider
    {
        private readonly IIocManager _iocManager;

        public ExchangeProvider(IIocManager iocManager)
        {
            _iocManager = iocManager;
        }

        public Api.Exchange.Exchange Get(string id)
        {
            ///На тип Exchange зарегистрировано много бирж с назличными именами.
            /// В качестве имени использовался идентификатор биржи
            var exchange = _iocManager.IocContainer.Resolve<Api.Exchange.Exchange>(id);

            return exchange;
        }

        public IList<Api.Exchange.Exchange> GetAll()
        {
            var exchanges = _iocManager.IocContainer.ResolveAll<Api.Exchange.Exchange>();

            return exchanges;
        }
    }
}
