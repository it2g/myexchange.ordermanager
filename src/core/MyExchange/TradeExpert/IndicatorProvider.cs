﻿using System.Collections.Generic;
using System.Linq;
using Abp.Application.Services;
using Abp.Dependency;
using MyExchange.Api.TradeExpert;

namespace MyExchange.TradeExpert
{
    /// <summary>
    /// Простейший поставщик экспертов тля торговых тактик
    /// </summary>
   public class IndicatorProvider : ApplicationService, IIndicatorProvider
   {
       private readonly IIocResolver _iocResolver;

        public IndicatorProvider(IIocResolver iocResolver)
       {
           _iocResolver = iocResolver;
       }
        /// <summary>
        /// По переданным параметрам производится подбор и фильтрация экспертов.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
       public  ITradeIndicator Get(string id)
        {
            var indicators = _iocResolver.ResolveAll<ITradeIndicator>();
            
            return indicators.FirstOrDefault(i => i.Id == id); ;
        }

        public IList<ITradeIndicator> GetIndicators(string stringIndicators)
        {
            var indicatorIds = stringIndicators.Split(",");
            
            var indicators = _iocResolver.ResolveAll<ITradeIndicator>();

            return indicators.Where(i=> indicatorIds.Contains(i.Id)).ToList();
        }

        public IList<ITradeIndicator> GetAll()
        {
            var indicators = _iocResolver.ResolveAll<ITradeIndicator>();

            return indicators;
        }
    }
}
