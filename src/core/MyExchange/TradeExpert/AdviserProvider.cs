﻿using System.Collections.Generic;
using System.Linq;
using Abp.Dependency;
using MyExchange.Api.Constants;
using MyExchange.Api.TradeExpert;
using MyExchange.Web.TradeExpert;

namespace MyExchange.TradeExpert
{
    /// <summary>
    /// Простейший поставщик экспертов тля торговых тактик
    /// </summary>
   public class AdviserProvider : AdviserProviderBase
   {
       private readonly IIocResolver _iocResolver;

       public AdviserProvider(IIocResolver iocResolver)
       {
           _iocResolver = iocResolver;
       }
        /// <summary>
        /// По переданным параметрам производится подбор и фильтрация экспертов.
        /// </summary>
        /// <param name="parametrs"></param>
        /// <returns></returns>
       public override IList<ITradeAdviser> GetAdvisers(IDictionary<string, object> parametrs)
       {
            var tradeAdvisers = _iocResolver.ResolveAll<ITradeAdviser>().ToList();

             object tradingTypeParam = null;

            parametrs.TryGetValue("TradingType", out tradingTypeParam);

           if (tradingTypeParam != null)
           {
               var tradingType =  (TradingTypes)tradingTypeParam;

                //здесь нужно подобрать подходящих советников для торговой тактики сделавщей запрос
                tradeAdvisers = tradeAdvisers.Where(e => e.TradingTypeList.Any(x => x == tradingType)).ToList();

           }
   
            return tradeAdvisers;
        }
    }
}
