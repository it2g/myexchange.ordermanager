﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyExchange.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "core");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                name: "bars",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    bar_time = table.Column<DateTime>(nullable: false),
                    height = table.Column<decimal>(nullable: false),
                    close = table.Column<decimal>(nullable: false),
                    open = table.Column<decimal>(nullable: false),
                    low = table.Column<decimal>(nullable: false),
                    value = table.Column<decimal>(nullable: false),
                    time_frame = table.Column<TimeSpan>(nullable: false),
                    exchange_id = table.Column<string>(nullable: false),
                    pair_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bars", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "currencies",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    brif_name = table.Column<string>(nullable: false),
                    full_name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_currencies", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "exchange_user",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    external_id = table.Column<string>(nullable: false),
                    user_id = table.Column<long>(nullable: false),
                    exchange_id = table.Column<string>(nullable: false),
                    api_secret_key = table.Column<string>(nullable: true),
                    salt = table.Column<string>(nullable: true),
                    api_open_key = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_exchange_user", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "trade_expert_advices",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    advice = table.Column<int>(nullable: false),
                    possibility = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_trade_expert_advices", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "trade_expert_liquidities",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    trade_expert_id = table.Column<Guid>(nullable: false),
                    liquidity = table.Column<double>(nullable: false),
                    date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_trade_expert_liquidities", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "wallet",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    user_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_wallet", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "pairs",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    currency1_id = table.Column<Guid>(nullable: false),
                    currency2_id = table.Column<Guid>(nullable: false),
                    id_deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_pairs", x => x.id);
                    table.ForeignKey(
                        name: "FK_pairs_currencies_currency1_id",
                        column: x => x.currency1_id,
                        principalSchema: "core",
                        principalTable: "currencies",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_pairs_currencies_currency2_id",
                        column: x => x.currency2_id,
                        principalSchema: "core",
                        principalTable: "currencies",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "orders",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    order_id = table.Column<string>(nullable: true),
                    pair_id = table.Column<Guid>(nullable: false),
                    exchange_id = table.Column<string>(nullable: true),
                    date = table.Column<DateTime>(nullable: false),
                    price = table.Column<decimal>(nullable: false),
                    quantity = table.Column<decimal>(nullable: false),
                    trade_type = table.Column<int>(nullable: false),
                    order_status = table.Column<int>(nullable: false),
                    is_deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_orders", x => x.id);
                    table.ForeignKey(
                        name: "FK_orders_pairs_pair_id",
                        column: x => x.pair_id,
                        principalSchema: "core",
                        principalTable: "pairs",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "pair_info_on_exchange",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    pair_id = table.Column<Guid>(nullable: false),
                    exchange_id = table.Column<string>(nullable: false),
                    higt = table.Column<decimal>(nullable: false),
                    low = table.Column<decimal>(nullable: false),
                    avarage = table.Column<decimal>(nullable: false),
                    value = table.Column<decimal>(nullable: false),
                    value_curr = table.Column<decimal>(nullable: false),
                    last_trade = table.Column<decimal>(nullable: false),
                    buy_price = table.Column<decimal>(nullable: false),
                    sell_price = table.Column<decimal>(nullable: false),
                    updated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_pair_info_on_exchange", x => x.id);
                    table.ForeignKey(
                        name: "FK_pair_info_on_exchange_pairs_pair_id",
                        column: x => x.pair_id,
                        principalSchema: "core",
                        principalTable: "pairs",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "signals",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    price = table.Column<decimal>(nullable: false),
                    stoploss = table.Column<decimal>(nullable: false),
                    take_profit = table.Column<decimal>(nullable: false),
                    trend = table.Column<int>(nullable: false),
                    signal_time = table.Column<DateTime>(nullable: false),
                    indicator_id = table.Column<string>(nullable: false),
                    indicator_context_id = table.Column<Guid>(nullable: false),
                    pair_id = table.Column<Guid>(nullable: false),
                    exchange_id = table.Column<string>(nullable: false),
                    time_frame = table.Column<TimeSpan>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_signals", x => x.id);
                    table.ForeignKey(
                        name: "FK_signals_pairs_pair_id",
                        column: x => x.pair_id,
                        principalSchema: "core",
                        principalTable: "pairs",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "opened_orders",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    order_id = table.Column<Guid>(nullable: false),
                    user_id = table.Column<long>(nullable: false),
                    stop_loss = table.Column<decimal>(nullable: true),
                    take_profit = table.Column<decimal>(nullable: true),
                    trading_tactic_context_id = table.Column<Guid>(nullable: false),
                    time_frame = table.Column<TimeSpan>(nullable: false),
                    comment = table.Column<string>(nullable: true),
                    is_deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_opened_orders", x => x.id);
                    table.ForeignKey(
                        name: "FK_opened_orders_orders_order_id",
                        column: x => x.order_id,
                        principalSchema: "core",
                        principalTable: "orders",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "trades",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    trade_id = table.Column<string>(nullable: false),
                    exchange_id = table.Column<string>(nullable: false),
                    order_id = table.Column<Guid>(nullable: true),
                    price = table.Column<decimal>(nullable: false),
                    quantity = table.Column<decimal>(nullable: false),
                    amount = table.Column<decimal>(nullable: false),
                    pair_id = table.Column<Guid>(nullable: false),
                    date = table.Column<DateTime>(nullable: false),
                    trade_type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_trades", x => x.id);
                    table.ForeignKey(
                        name: "FK_trades_orders_order_id",
                        column: x => x.order_id,
                        principalSchema: "core",
                        principalTable: "orders",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_trades_pairs_pair_id",
                        column: x => x.pair_id,
                        principalSchema: "core",
                        principalTable: "pairs",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_opened_orders_order_id",
                schema: "core",
                table: "opened_orders",
                column: "order_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_orders_pair_id",
                schema: "core",
                table: "orders",
                column: "pair_id");

            migrationBuilder.CreateIndex(
                name: "IX_pair_info_on_exchange_pair_id",
                schema: "core",
                table: "pair_info_on_exchange",
                column: "pair_id");

            migrationBuilder.CreateIndex(
                name: "IX_pairs_currency1_id",
                schema: "core",
                table: "pairs",
                column: "currency1_id");

            migrationBuilder.CreateIndex(
                name: "IX_pairs_currency2_id",
                schema: "core",
                table: "pairs",
                column: "currency2_id");

            migrationBuilder.CreateIndex(
                name: "IX_signals_pair_id",
                schema: "core",
                table: "signals",
                column: "pair_id");

            migrationBuilder.CreateIndex(
                name: "IX_trades_exchange_id",
                schema: "core",
                table: "trades",
                column: "exchange_id");

            migrationBuilder.CreateIndex(
                name: "IX_trades_order_id",
                schema: "core",
                table: "trades",
                column: "order_id");

            migrationBuilder.CreateIndex(
                name: "IX_trades_pair_id",
                schema: "core",
                table: "trades",
                column: "pair_id");

            migrationBuilder.CreateIndex(
                name: "IX_trades_trade_id",
                schema: "core",
                table: "trades",
                column: "trade_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "bars",
                schema: "core");

            migrationBuilder.DropTable(
                name: "exchange_user",
                schema: "core");

            migrationBuilder.DropTable(
                name: "opened_orders",
                schema: "core");

            migrationBuilder.DropTable(
                name: "pair_info_on_exchange",
                schema: "core");

            migrationBuilder.DropTable(
                name: "signals",
                schema: "core");

            migrationBuilder.DropTable(
                name: "trade_expert_advices",
                schema: "core");

            migrationBuilder.DropTable(
                name: "trade_expert_liquidities",
                schema: "core");

            migrationBuilder.DropTable(
                name: "trades",
                schema: "core");

            migrationBuilder.DropTable(
                name: "wallet",
                schema: "core");

            migrationBuilder.DropTable(
                name: "orders",
                schema: "core");

            migrationBuilder.DropTable(
                name: "pairs",
                schema: "core");

            migrationBuilder.DropTable(
                name: "currencies",
                schema: "core");
        }
    }
}
