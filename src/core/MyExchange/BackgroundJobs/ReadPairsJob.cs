﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Hangfire;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Internal;
using MyExchange.Api.Dal;
using MyExchange.Api.Data;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Exchange;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;

namespace MyExchange.BackgroundJobs
{
    /// <summary>
    /// Фоновая задача на считывание из биржи новых валютных пар
    /// </summary>
    [Queue("feed_queue")]
    public class ReadPairsJob : BackgroundJob<string>, ITransientDependency
    {
        private readonly IPairCrudAppService _pairCrudService;
        private readonly IPairRepository _pairRepository;
        private readonly IPairInfoOnExchangeRepository _pairInfoOnExchangeRepository;
        private readonly ICurrencyRepository _currencyRepository;
        private readonly IExchangeProvider _exchangeProvider;
        private readonly IPairInfoOnExchangeCrudAppService _pairInfoOnExchangeCrudAppService;
        private static object lockerCreatePair = new object();
        private static object lockerCreatePainInfo = new object();


        public ReadPairsJob([NotNull] IPairCrudAppService pairCrudService,
                            [NotNull] IPairRepository pairRepository,
                            [NotNull] IExchangeProvider exchangeProvider,
                            [NotNull] IPairInfoOnExchangeCrudAppService pairInfoOnExchangeCrudAppService,
                            [NotNull] ICurrencyRepository currencyRepository,
                            [NotNull] IPairInfoOnExchangeRepository pairInfoOnExchangeRepository)
        {
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _pairInfoOnExchangeCrudAppService = pairInfoOnExchangeCrudAppService ?? throw new ArgumentNullException(nameof(pairInfoOnExchangeCrudAppService));
            _currencyRepository = currencyRepository ?? throw new ArgumentNullException(nameof(currencyRepository));
            _pairInfoOnExchangeRepository = pairInfoOnExchangeRepository ?? throw new ArgumentNullException(nameof(pairInfoOnExchangeRepository));
            _pairCrudService = pairCrudService ?? throw new ArgumentNullException(nameof(pairCrudService));
            _pairRepository = pairRepository ?? throw new ArgumentNullException(nameof(pairRepository));
        }
        [UnitOfWork(IsDisabled = true)]
        public override void Execute(string exchangeId)
        {
            var exchange = _exchangeProvider.Get(exchangeId);

            IEnumerable<PairSettings> pairSettingsList = exchange.ExchangeDriver.PairSettingsAsync().Result;

            foreach (PairSettings pairSettings in pairSettingsList)
            {
                PairDto pairDto;

                lock (lockerCreatePair)
                {
                    //Изменить условие поиска пары по наименованиям валют
                    var pairs = _pairCrudService.GetAll(new PairPagedResultRequestDto { PairSettings = pairSettings }).Result;

                    if (!pairs.Items.Any())
                    {
                        //Нужен UoW т.к. поднимаются валюты и создается пара. Все должно
                        //производится в едином контексте данных
                        using (var unitOfWork = UnitOfWorkManager.Begin())
                        {
                            //Создается валютная пара если она отсутсвует в БД
                            var currency1 = _currencyRepository.FirstOrDefault(
                                cur => cur.BrifName.ToUpper() == pairSettings.Pair.Currency1.BrifName.ToUpper());
                            var currency2 = _currencyRepository.FirstOrDefault(
                                cur => cur.BrifName.ToUpper() == pairSettings.Pair.Currency2.BrifName.ToUpper());

                            if (currency1 == null || currency2 == null)
                            {
                                Logger.Debug("При сохранении валютной пары не обнаружилась валюта, сохраненная в БД");
                                continue;
                            }

                            pairSettings.Pair.Currency1 = currency1.MapTo<CurrencyDto>();
                            pairSettings.Pair.Currency2 = currency2.MapTo<CurrencyDto>();
                            pairSettings.Pair.Id = Guid.NewGuid();

                            pairDto = _pairCrudService.Create(pairSettings.Pair.MapTo<PairDto>()).Result;

                            Console.WriteLine($"Добавлена новая валютная пара {pairDto} в БД");

                            unitOfWork.Complete();
                        }
                    }
                    else
                    {
                        pairDto = pairs.Items.First();

                        Console.WriteLine($"Валютная пара {pairDto} уже существует в БД");
                    }
                }

                lock (lockerCreatePainInfo)
                {
                    var pairInfoOnExchange = _pairInfoOnExchangeRepository
                        .FirstOrDefault(p => p.PairId == pairDto.Id && p.ExchangeId == exchangeId);

                    if (pairInfoOnExchange == null)
                    {
                        _pairInfoOnExchangeCrudAppService.Create(new PairInfoOnExchangeDto
                        {
                            PairId = pairDto.Id,
                            ExchangeId = exchangeId
                        }).Wait();
                    }
                    else
                    {
                        Console.WriteLine($"Валютная пара {pairDto} уже имеется в БД");
                    }
                }
            }

            Console.WriteLine("Добавление новых валютных пар в БД завершино");
        }
    }
}
