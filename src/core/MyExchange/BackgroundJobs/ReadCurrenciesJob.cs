﻿using System;
using System.Collections.Generic;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Hangfire;
using JetBrains.Annotations;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Exchange;
using MyExchange.Api.Services.Crud;

namespace MyExchange.BackgroundJobs
{
    /// <summary>
    /// Фоновая задача для считывания валют с указанной биржи
    /// </summary>
    [Queue("feed_queue")]
    public class ReadCurrenciesJob : BackgroundJob<string>, ITransientDependency
    {
        private IExchangeProvider _exchangeProvider;
        private ICurrencyRepository _currencyRepository;
        private readonly ICurrencyCrudAppService _currencyCrudService;
        private static object locker = new object();

        public ReadCurrenciesJob([NotNull] IExchangeProvider exchangeProvider,
            [NotNull] ICurrencyRepository currencyRepository,
            [NotNull] ICurrencyCrudAppService currencyCrudService)
        {
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _currencyRepository = currencyRepository ?? throw new ArgumentNullException(nameof(currencyRepository));
            _currencyCrudService = currencyCrudService ?? throw new ArgumentNullException(nameof(currencyCrudService));
        }

        [UnitOfWork(IsDisabled = true)]
        public override void Execute(string exchangeId)
        {
            var exchange = _exchangeProvider.Get(exchangeId);

            IEnumerable<CurrencyDto> currencies = exchange.ExchangeDriver.CurrenciesAsync().Result;

            foreach (CurrencyDto currency in currencies)
            {
                ///Обязательно нужна блокировка на две операции чтения и записи, чтобы разные потоки
                /// не могли записать одну и ту же валюту в БД
                lock (locker)
                {
                    var curr = _currencyRepository.FirstOrDefault(x => x.BrifName.ToUpper() == currency.BrifName.ToUpper());

                    if (curr == null)
                    {
                        currency.BrifName = currency.BrifName.ToUpper();
                        currency.Id = Guid.NewGuid();

                        var currencyDto = _currencyCrudService.Create(currency.MapTo<CurrencyDto>()).Result;

                        Console.WriteLine($"Добавлена валюта {currencyDto.BrifName.ToUpper()} в БД");
                    }
                    else
                    {
                        Console.WriteLine($"Валюта {currency.BrifName.ToUpper()} уже имеется в БД");
                    }
                }
            }

            Console.WriteLine("Добавление валют в БД завершено");
        }
    }
}
