﻿using MyExchange.Api.Entity;
using System.Threading.Tasks;
using MyExchange.Api.Dto.OpenedOrder;

namespace MyExchange.Api.OrderManagment
{
    public interface ITrailingStopCalculator 
    {
        Task<decimal> CalculateTakeProfitAsync(OpenedOrderDto order);
    }
}
