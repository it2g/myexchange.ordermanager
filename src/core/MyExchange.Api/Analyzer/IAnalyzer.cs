﻿using System;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;

namespace MyExchange.Api.TradeExpert
{
    /// <summary>
    /// Эксперт в области торговли основанный на одном из устойчивых паттернов
    /// </summary>
    public interface IAnalyzer: IBotTactic
    {
       
    }
    public interface IAnalyzer<TContext>: IBotTactic<TContext>, IAnalyzer
        where TContext : BotTacticContextBase
    {
    }
}
