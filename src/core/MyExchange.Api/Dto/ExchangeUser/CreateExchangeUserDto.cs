﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Runtime.Validation;


namespace MyExchange.Api.Dto.ExchangeUser
{
   public class CreateExchangeUserDto : IShouldNormalize
    {
       
        public string ExternalId { get; set; }

        public long UserId { get; set; }

        /// <summary>
        /// Биржа, на которой зарегистрирован пользователь
        /// </summary>
        public string ExchangeId { get; set; }

        /// <summary>
        /// Секретный ключ для доступа к api биржи
        /// </summary>
        private string _apiSecretKey;
        //TODO реализовать безопасное хренение секретного ключа механизмом шифрации
        //https://docs.microsoft.com/ru-ru/dotnet/standard/security/cryptography-model
        public string ApiSecretKey
        {
            get
            {
                return _apiSecretKey;
            }
            set
            {
                _apiSecretKey = value;
            }
        }

        /// <summary>
        /// Открытый ключ для шифрования и безопасного хранения в БД  закрытого ключа
        /// </summary>
        public string Salt { get; set; }
        /// <summary>
        /// Открытый ключ для шифрования отправляемых сообщений на биржу
        /// </summary>
        public string ApiOpenKey { get; set; }

       public void Normalize()
       {

       }
    }
}
