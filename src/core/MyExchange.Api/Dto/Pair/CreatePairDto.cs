﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Runtime.Validation;

namespace MyExchange.Api.Dto.Pair
{
   public class CreatePairDto : IShouldNormalize
    {
        public Guid Currency1Id { get; set; }

        public Guid Currency2Id { get; set; }

        public void Normalize()
        {
        }
    }
}
