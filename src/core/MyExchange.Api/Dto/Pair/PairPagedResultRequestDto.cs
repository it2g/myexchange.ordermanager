﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using MyExchange.Api.Data;

namespace MyExchange.Api.Dto.Pair
{
   public class PairPagedResultRequestDto : PagedAndSortedResultRequestDto
   {
       public PairSettings PairSettings { get; set; }
   }
}
