﻿using Abp.Application.Services.Dto;
using MyExchange.Api.Constants;
using System;

namespace MyExchange.Api.Dto.OpenedOrder
{
    public class OpenedOrderPagedResultRequestDto 
    {
        public string ExchangeId { get; set; }

        /// <summary>
        /// Объем выручки, котрая получена в процессе работы бота
        /// </summary>
        public decimal? Value { get; set; }

        public Guid? PairId { get; set; }

        public OrderStatuses? Status { get; set; }
    }
}
