﻿using System;
using MyExchange.Api.Dto.Order;
using MyExchange.Api.Dto._DtoBase;

namespace MyExchange.Api.Dto.OpenedOrder
{
    /// <summary>
    /// 
    /// </summary>
    public class OpenedOrderDto : DtoBase
    {
        public OrderDto Order { get; set; }

        public Guid OrderId { get; set; }

        public long UserId { get; set; }

        public decimal? StopLoss { get; set; }

        public decimal? TakeProfit { get; set; }
        
        public Guid TradingTacticContextId { get; set; }

        public TimeSpan TimeFrame { get; set; }
       
        public string Comment { get; set; }

    }
}
