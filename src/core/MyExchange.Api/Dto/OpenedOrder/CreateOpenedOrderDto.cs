﻿using System;

namespace MyExchange.Api.Dto.OpenedOrder
{
    public class CreateOpenedOrderDto
    {
        public Guid OrderId { get; set; }
        
        public long UserId { get; set; }

        public decimal StopLoss { get; set; }

        public decimal TakeProfit { get; set; }

        public Guid TradingTacticContextId { get; set; }

        public TimeSpan TimeFrame { get; set; }

        public string Comment { get; set; }

    }
}
