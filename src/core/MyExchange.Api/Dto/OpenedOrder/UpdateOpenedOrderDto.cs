﻿namespace MyExchange.Api.Dto.OpenedOrder
{
    public class UpdateOpenedOrderDto 
    {
        public decimal StopLoss { get; set; }

        public decimal TakeProfit { get; set; }

        public string Comment { get; set; }

    }
}
