﻿using MyExchange.Api.Dto._DtoBase;
using MyExchange.Api.TradeExpert;

namespace MyExchange.Api.Dto.TradeExpertAdvice
{
   public class TradeExpertAdviceDto : DtoBase
    {
       public Advices Advice { get; set; }

       /// <summary>
       /// Вероятность сделанной оценки по мнению эксперта
       /// </summary>
       public double Possibility { get; set; }
    }
}
