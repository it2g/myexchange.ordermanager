﻿using System;
using MyExchange.Api.Dto._DtoBase;

namespace MyExchange.Api.Dto.CurrencyUserBalance
{
    public class CurrencyUserBalanceDto : DtoBase
    {
        public Guid CurrencyId { get; set; }

        public Guid WalletId { get; set; }

        /// <summary>
        /// Баланс валюты на кошельке
        /// </summary>
        public decimal Value { get; set; }
    }
}