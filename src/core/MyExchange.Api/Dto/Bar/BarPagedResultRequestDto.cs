﻿using System;
using Abp.Application.Services.Dto;

namespace MyExchange.Api.Dto.Bar
{
   public class BarPagedResultRequestDto : PagedAndSortedResultRequestDto
   {
       public Guid? PairId { get; set; }
       public string ExchangeId { get; set; }
       public TimeSpan? TimeFrame { get; set; }
       public DateTime? CurrentTime { get; set; }
       public int? CountBars { get; set; }
       public override int MaxResultCount { get; set; } = 1000000;

   }
}
