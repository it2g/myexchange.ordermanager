﻿using System;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto._DtoBase;

namespace MyExchange.Api.Dto.Bar
{
    /// <summary>
    /// Свеча 
    /// </summary>
    public class BarDto : DtoBase
    {
        /// <summary>
        /// Время к которому привязано начало открытия свечи
        /// </summary>
        public DateTime BarTime { get; set; }

        /// <summary>
        /// Самая выскоая цена сделки в рамках свечи
        /// </summary>
        public decimal Height { get; set; }

        /// <summary>
        /// Цена последней сделки в интервале свечи
        /// </summary>
        public decimal Close { get; set; }
        
        /// <summary>
        /// Цена первой сделки в интервале свечи
        /// </summary>
        public decimal Open { get; set; }
        
        /// <summary>
        /// Самая низкая цена сделки в рамках свечи
        /// </summary>
        public decimal Low { get; set; }

        /// <summary>
        /// Объем свечи в денежном выражении
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// Рамерность свечи по продолжительности времени
        /// </summary>
        public TimeSpan TimeFrame { get; set; }
       
        public string ExchangeId { get; set; }

        public Guid PairId { get; set; }

        /// <summary>
        /// Тренд свечи
        /// TODO оставить, если этот параметр пригодиться в рассчетах.
        /// </summary>
        public Trends Trend => Open > Close ? Trends.Downtrend :
            Open < Close ? Trends.Uptrend : Trends.Sideways;
    }
}
