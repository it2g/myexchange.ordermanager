﻿using System;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto._DtoBase;

namespace MyExchange.Api.Dto.Trade
{
    /// <summary>
    /// Сделка по валютной паре
    /// </summary>
    public class TradeDto : DtoBase
    { 
        public string TradeId { get; set; }

        public string ExchangeId { get; set; }

        public string ExternalOrderId { get; set; }

        public Guid?  OrderId { get; set; }

        public decimal Price { get; set; }

        public decimal Quantity { get; set; }

        public decimal Amount { get; set; }

        public DateTime Date { get; set; }

        public TradeTypes TradeTypes { get; set; }

        public Guid PairId { get; set; }

        

    }

   
}
