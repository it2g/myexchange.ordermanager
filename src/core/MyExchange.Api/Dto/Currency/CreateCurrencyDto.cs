﻿using Abp.Runtime.Validation;

namespace MyExchange.Api.Dto.Currency
{
    public class CreateCurrencyDto : IShouldNormalize
    {
        public string BrifName { get; set; }

        public string FullName { get; set; }

        public void Normalize()
        { }
    }
}
