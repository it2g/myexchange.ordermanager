﻿using MyExchange.Api.Dto._DtoBase;

namespace MyExchange.Api.Dto.Currency
{
    public class CurrencyDto : DtoBase
    {
        public string BrifName { get; set; }

        public string FullName { get; set; }
    }
}
