﻿using System;
using MyExchange.Api.Dto._DtoBase;
using MyExchange.Api.Dto.Pair;

namespace MyExchange.Api.Dto.PairInfoOnExchange
{
    public class PairInfoOnExchangeDto : DtoBase
    {
        public Guid PairId { get; set; }
        public PairDto Pair { get; set; }

        /// <summary>
        /// Биржа, с которой получена информация о валютной паре
        /// </summary>
        public string ExchangeId { get; set; }

        /// <summary>
        /// Mаксимальная цена сделки за 24 часа
        /// </summary>
        public decimal High { get; set; }
        /// <summary>
        /// Минимальная цена сделки за 24 часа
        /// </summary>
        public decimal Low { get; set; }

        /// <summary>
        /// Средняя цена сделки за 24 часа
        /// </summary>
        public decimal Avarage { get; set; }

        /// <summary>
        /// Объем всех сделок за 24 часа
        /// </summary>
        public ulong Value { get; set; }

        /// <summary>
        /// Сумма всех сделок за 24 часа
        /// </summary>
        public decimal ValueCurr { get; set; }

        /// <summary>
        /// Цена последней сделки
        /// </summary>
        public decimal LastTrade { get; set; }

        /// <summary>
        /// Текущая максимальная цена покупки
        /// </summary>
        public decimal BuyPrice { get; set; }

        /// <summary>
        /// Текущая минимальная цена продажи
        /// </summary>
        public decimal SellPrice { get; set; }

        /// <summary>
        /// Дата и время обновления данных
        /// </summary>
        public DateTime Updated { get; set; }
    }
}
