﻿using System.Collections.Generic;
using Abp.Application.Services;

namespace MyExchange.Api.Exchange
{
    /// <summary>
    /// Предоставляет биржи 
    /// </summary>
    public interface IExchangeProvider : IApplicationService
    {
        Api.Exchange.Exchange Get(string id);

        IList<Api.Exchange.Exchange> GetAll();
    }
}
