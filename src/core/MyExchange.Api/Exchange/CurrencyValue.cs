﻿using MyExchange.Api.Entity;

namespace MyExchange.Api.Exchange
{
    /// <summary>
    /// Объем денежных средств в определенной валюте, котрый может быть
    /// передан/возвращен ботом
    /// </summary>
    public class CurrencyValue
    {
        public Currency Currency {get; set; }

        public decimal Value { get; set; }
    }
}
