﻿using System;
using System.Collections.Generic;
using MyExchange.Api.Dto.Currency;

namespace MyExchange.Api.Exchange
{
    /// <summary>
    /// Баланс кошелька пользователя 
    /// </summary>
    public class ExchangeUserInfo
    {
        public ExchangeUserInfo()
        {
            Balances = new Dictionary<CurrencyDto, decimal>();
            Reserved = new Dictionary<CurrencyDto, decimal>();
        }
        public Guid ExchangeUserId { get; set; }

        public DateTime InfoReciveDate { get; set; }
        
        /// <summary>
        /// Доступный баланс на счету пользователя по валютам
        /// TODO Не понятно как предполагается записывать в БД это и следующее всойство?
        /// </summary>
        public IDictionary<CurrencyDto, Decimal> Balances { get; }

        /// <summary>
        /// Баланс пользователя в ордерах
        /// </summary>
        public IDictionary<CurrencyDto, Decimal> Reserved { get; }
    
    }
}
