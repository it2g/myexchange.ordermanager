﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.Services.Infrastructure
{
    /// <summary>
    /// Настройки для создания экземпляра таймера
    /// </summary>
    public class TimerOptions
    {
        /// <summary>
        /// Нужна ли трансформация времени
        /// </summary>
        public bool NeedRelativeTime { get; set; }
        /// <summary>
        /// Точка отчета времени в замен DataTime.UtcNow
        /// на момент инициализации таймера
        /// </summary>
        public DateTime RelativeTimeReference { get; set; }

        /// <summary>
        /// Масштабный коэф-нт времени, котрый позволит ускорять процесс
        /// обработки данных
        /// </summary>
        public uint Scale { get; set; }

        public TimerOptions()
        {
            RelativeTimeReference = DateTime.UtcNow;
            Scale = 1;
        }
    }
}
