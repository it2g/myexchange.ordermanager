﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.Services.Infrastructure
{
    /// <summary>
    /// Таймер предоставляет текущее время и интервалы времени
    /// с учетом установленной точки отчета времени и масштабного
    /// коэффициента
    /// </summary>
    public interface ITimer 
    {

        /// <summary>
        /// Вычисляется дата и время с учетом смешения координаты текущейго времени
        /// и используемого масштабного коэф-та времени
        /// </summary>
        DateTime CurrentTime();

        /// <summary>
        /// Вычисляется интервал времени с учетом импользуемого масштабного коэф-та.
        /// Может применяться для задержки, ожидания или скачков во времени.
        /// </summary>
        /// <param name="millisecond"></param>
        /// <returns></returns>
        TimeSpan SpeedUpInMilliSeconds(int millisecond);

        TimeSpan SpeedUpInSeconds(double second);

        TimeSpan SpeedUpInMinutes(double minutes);

        TimeSpan SpeedUpInHоurs(int hours);
        
        TimeSpan SpeedUp(TimeSpan time);

        TimeSpan SlowDoun(TimeSpan time);

    }
}
