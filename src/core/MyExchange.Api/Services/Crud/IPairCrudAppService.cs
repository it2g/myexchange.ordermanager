﻿using System;
using System.Threading.Tasks;
using MyExchange.Api.Dto.Pair;

namespace MyExchange.Api.Services.Crud
{
    public interface IPairCrudAppService : IExhchangeCrudAppServices<PairDto, Guid, PairPagedResultRequestDto, PairDto, PairDto, PairDto, PairDto>
    {
        Task<Guid> GetPairId(string currency1Name, string currency2Name);
    }
}
