﻿using System;
using MyExchange.Api.Data;
using MyExchange.Api.Dto.Bar;

namespace MyExchange.Api.Services.Crud
{
    public interface IBarCrudAppService : IExhchangeCrudAppServices<BarDto, Guid, BarPagedResultRequestDto, BarDto, BarDto, BarDto, BarDto>
    {
    }
}
