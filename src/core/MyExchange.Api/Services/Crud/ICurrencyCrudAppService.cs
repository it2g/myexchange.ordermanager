﻿using System;
using MyExchange.Api.Dto.Currency;

namespace MyExchange.Api.Services.Crud
{
    public interface ICurrencyCrudAppService : IExhchangeCrudAppServices<CurrencyDto,  Guid, CurrencyPagedResultRequestDto, CurrencyDto, CurrencyDto, CurrencyDto, CurrencyDto>
    {
       
    }
}
