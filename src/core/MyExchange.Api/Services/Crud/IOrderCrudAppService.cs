﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyExchange.Api.Dto.Order;

namespace MyExchange.Api.Services.Crud
{
    public interface IOrderCrudAppService : IExhchangeCrudAppServices<OrderDto, Guid, OrderPagedResultRequestDto, OrderDto, OrderDto, OrderDto,OrderDto>
    {
        /// <summary>
        /// Принимает список ордеров считанных с биржи и находит среди них те, котрых еще нет в БД
        /// сравнимая по внешним идентификаторам и добавляет их в БД
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        Task<IList<OrderDto>> AddList(IList<OrderDto> list);
    }
    
}
