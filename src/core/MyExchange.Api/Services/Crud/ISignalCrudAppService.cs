﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Dto.Signal;
using MyExchange.Api.Dto.Trade;

namespace MyExchange.Api.Services.Crud
{
    public interface ISignalCrudAppService : IExhchangeCrudAppServices<SignalDto, Guid, SignalPagedResultRequestDto, SignalDto, SignalDto, SignalDto, SignalDto>
    {
    }
}
