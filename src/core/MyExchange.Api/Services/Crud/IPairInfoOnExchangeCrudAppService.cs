﻿using System;
using MyExchange.Api.Dto.PairInfoOnExchange;

namespace MyExchange.Api.Services.Crud
{
    public interface IPairInfoOnExchangeCrudAppService : IExhchangeCrudAppServices<PairInfoOnExchangeDto, Guid, PairInfoOnExchangePagedResultRequestDto, PairInfoOnExchangeDto, PairInfoOnExchangeDto, PairInfoOnExchangeDto, PairInfoOnExchangeDto>
    {
    }
    
}
