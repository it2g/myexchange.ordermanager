﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using MyExchange.Api.Data;
using MyExchange.Api.Dto.Bar;

namespace MyExchange.Api.Services.Bl
{
    /// <summary>
    /// Предоставляет сервис управления свечами
    /// </summary>
    public interface IBarProvider
    {
        /// <summary>
        /// Предоставляет список свечей указанной размерности для валюты
        /// не конкретной бирже 
        /// </summary>/// <param name="pairId"></param>
        /// <param name="exchangeId"></param>
        /// <param name="timeFrame">временной размер свечи</param>
        /// <param name="begin">начало временного диапазона</param>
        /// <param name="end">окончание временного диапазона</param>
        /// <returns></returns>
        Task<IList<BarDto>> GetList(Guid pairId, string exchangeId, TimeSpan timeFrame, DateTime begin, DateTime end, int countBars = 0);
        /// <summary>
        /// Возвращает все свечи из указанного диапазона
        /// </summary>
        /// <param name="pairId"></param>
        /// <param name="exchangeId"></param>
        /// <param name="timeFrame"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        Task<IList<BarDto>> GetList(Guid pairId, string exchangeId, TimeSpan timeFrame, DateTime begin, DateTime end);
        Task<IList<BarDto>> GetList(Guid pairId, string exchangeId, TimeSpan timeFrame, int countBars = 0);


    }
}
