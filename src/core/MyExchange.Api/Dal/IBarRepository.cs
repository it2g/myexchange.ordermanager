﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Repositories;
using MyExchange.Api.Data;
using MyExchange.Api.Entity;

namespace MyExchange.Api.Dal
{
   public interface IBarRepository : IRepository<Bar, Guid>
    {
    }
}
