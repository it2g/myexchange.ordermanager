﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Entity;

namespace MyExchange.Api.Dal.EfConfigurations
{
    public abstract class BaseEntityMap<TEntity> : IEntityTypeConfiguration<TEntity>
        where TEntity : EntityBase
    {
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.Property(u => u.Id).HasColumnName("id");
            builder.Property(x => x.CreationTime).HasColumnName("creation_time");
        }
    }
}
