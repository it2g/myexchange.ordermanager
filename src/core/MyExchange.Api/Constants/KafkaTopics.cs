﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.Constants
{
    /// <summary>
    /// Набор методов, возвращающих значения топиков.
    /// Реализация в виде методов позволяет расширять этот класс
    /// добавляя в него новые топики, характерные для др. модулей
    /// </summary>
    public  class KafkaTopics 
    {
        /// <summary>
        /// Топик для сообщенией в шину данных
        /// </summary>
        public  string GetEventBusTopicForHeartBeat() => "heard_beat";
        
        /// <summary>
        /// Название топика, который будет помечать события формирования ордеров
        /// для передачи их менеджеру ордеров
        /// </summary>
        public  string GetEventBusTopicForOrderManager() => "order_for_order_manager";
        
        /// <summary>
        /// Топик, по которому индикатор может публиковать сообщения в Kafka
        /// для торговых тактик
        /// </summary>
        public  string GetEventBusTopicForTradingTactic() => "indicator_for_trading_tactic";

        
    }
}
