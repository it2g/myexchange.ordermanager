﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.Constants
{
    /// <summary>
    /// Тренд (от англ. trend — тенденция) — это долговременная тенденция изменения
    ///  исследуемого временного ряда. Тренды могут быть описаны 
    /// различными уравнениями — линейными, логарифмическими, степенными и так далее.
    /// </summary>
    public enum Trends
    {
        /// <summary>
        /// Флэт (горизонтальный, боковой) — тренд отсутствует — движение наблюдается в горизонтальном диапазоне.
        /// </summary>
        Sideways = 0,

        /// <summary>
        /// Повышательный (восходящий, бычий) — рынок растет;
        /// </summary>
        Uptrend = 1,

        /// <summary>
        /// Понижательный (нисходящий, медвежий) — рынок падает;
        /// </summary>
        Downtrend = 2
    }
}
