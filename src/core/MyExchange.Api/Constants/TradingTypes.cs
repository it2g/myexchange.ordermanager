﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.Constants
{
    /// <summary>
    /// Это типы торговли, необходимые для поставщика Экспертов, чтоб определять наиболее 
    /// подходящих экспертов для тактики торговли.
    /// </summary>
    public enum TradingTypes
    {
        ShortTime = 1,

        MidleTime = 2,

        LongTime = 3
    }
}
