﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Базовый класс для всех сущностей
    /// </summary>
    public class EntityBase : Entity<Guid>, IHasCreationTime
    {
        public DateTime CreationTime { get; set; }
    }
}
