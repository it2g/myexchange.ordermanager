﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Entities;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Баланс на криптовалютном кошельке пользователя
    /// </summary>
    public class CurrencyUserBalance : EntityBase
    {
        /// <summary>
        /// Валюта
        /// </summary>
        public Currency Currency { get; set; }
        public Guid CurrencyId { get; set; }

        public Wallet Wallet { get; set; }
        public Guid WalletId { get; set; }

        /// <summary>
        /// Баланс валюты на кошельке
        /// </summary>
        public decimal Value { get; set; }

    }
}
