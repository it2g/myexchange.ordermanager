﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MyExchange.Api.Constants;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Ордер открытый для покупки или продажи ботом или пользователем
    /// </summary>
    public class OpenedOrder : EntityBase, ISoftDelete
    {
        public virtual Order Order { get; set; }
       
        public Guid OrderId { get; set; }

        //Тот, кто поставил ордер
        public long UserId { get; set; }

        public decimal? StopLoss { get; set; }

        public decimal? TakeProfit { get; set; }

        public Guid TradingTacticContextId { get; set; }
         
        public TimeSpan TimeFrame { get; set; }

        public string Comment { get; set; }

        public bool IsDeleted { get; set; }
    }
}
