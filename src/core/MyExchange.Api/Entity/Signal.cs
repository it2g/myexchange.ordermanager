﻿using MyExchange.Api.Constants;
using System;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Сигнал на вход в рынок
    /// </summary>
    public class Signal : EntityBase
    {
        public decimal Price { get; set; }

        public decimal Stoploss { get; set; }

        public decimal TakeProfit { get; set; }

        public Trends Trend { get; set; }

        /// <summary>
        /// Когда сгенерирован сигнал
        /// </summary>
        public DateTime SignalTime { get; set; }

        public string IndicatorId { get; set; }
        
        public Guid IndicatorContextId { get; set; }

        public virtual Pair Pair { get; set; }
        public Guid PairId { get; set; }

        public string ExchangeId { get; set; }

        public TimeSpan TimeFrame { get; set; }
    }
}
