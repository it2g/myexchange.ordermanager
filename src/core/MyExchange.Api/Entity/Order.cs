﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Entities;
using MyExchange.Api.Constants;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Ордер на выполнение операции на бирже
    /// </summary>
    public class Order : EntityBase, ISoftDelete
    {
        /// <summary>
        /// Идентификатор из внешней системы (как правило - биржи)
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// Валютная пара
        /// </summary>
        public Guid PairId { get; set; }
        public virtual Pair Pair { get; set; }

        public  string ExchangeId { get; set; }

        public DateTime Date { get; set; }

        /// <summary>
        /// Цена покупки/продажи
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Объем валюты выставленный на покупку или продажу
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// Тип сделки
        /// </summary>
        public TradeTypes TradeTypes { get; set; }

        /// <summary>
        /// Состояние выставленного ордера
        /// </summary>
        public OrderStatuses OrderStatus { get; set; }

        public bool IsDeleted { get; set; }
    }
}
