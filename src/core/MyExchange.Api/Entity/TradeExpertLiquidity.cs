﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Ликвидность эксперта на определенный период времени
    /// Эта сущность должна показывать, насколько тот или иной эксперт дает дельные советы
    /// </summary>
    public class TradeExpertLiquidity : EntityBase
    {
        public Guid TradeExpertId { get; set; }

        public double Liquidity { get; set; }

        public DateTime Date { get; set; }
    }
}
