﻿using System;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Constants;

namespace MyExchange.Api.TradingStrategy
{
    public interface ITradingStrategyContextBase
    {
        /// <summary>
        /// Идентификатор пользователя, запустившего торговлю
        /// </summary>
        long UserId { get; set; }

        /// <summary>
        /// Спепень риска указанная пользователем
        /// </summary>
        RiskDegrees RiskDegree { get; set; }

        /// <summary>
        /// Продолжительность торговли заказанная пользователем
        /// </summary>
        TimeSpan Duration { get; set; }

        /// <summary>
        /// Используется менеджером ботов для мониторинга состояния бота
        /// </summary>
        BotStatuses Status { get; set; }

        /// <summary>
        /// Дата и время запуска бота
        /// Если null но запускать сразу же как появиться возможность
        /// </summary>
        DateTime? StartTime { get; set; }

        /// <summary>
        /// Дата и время остановки бота
        /// Если null то будет работать всегда, пока не остановится принудительно 
        /// оператором или использующей его стратегией торговли
        /// /summary>
        DateTime? StopTime { get; set; }

        Guid Id { get; set; }
    }

    public abstract class TradingStrategyContextBase : BotTacticContextBase, ITradingStrategyContextBase
    {
        /// <summary>
        /// Идентификатор пользователя, запустившего торговлю
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Спепень риска указанная пользователем
        /// </summary>
        public RiskDegrees RiskDegree { get; set; }

        /// <summary>
        /// Продолжительность торговли заказанная пользователем
        /// </summary>
        public TimeSpan Duration { get; set; }

        ///TODO Определить объем выделенных средств
    }
}
