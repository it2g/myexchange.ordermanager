﻿using System;
using Abp.Application.Services;

namespace MyExchange.Api.TradingStrategy
{
    public interface ITradingStrategyBuilder : IApplicationService
    {
        ITradingStrategy Create(Type botStrategyType, TradingStrategyContextBase BotStrategyContext);
    }
}
