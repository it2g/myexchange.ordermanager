﻿using System;
using System.Threading;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;

namespace MyExchange.Api.BotTactic
{
    /// <summary>
    /// Контекст, которым оперирует бот, выполняемая какую либо работу в фоновом режиме
    /// Этим контекстом оперируют большая часть тактик, но он может быть расширен наследованием
    ///  </summary>
    public abstract class BotTacticContextBase : EntityBase
    {
        /// <summary>
        /// Используется менеджером ботов для мониторинга состояния бота
        /// </summary>
        public BotStatuses Status { get; set; }

        /// <summary>
        /// Дата и время запуска бота
        /// Если null но запускать сразу же как появиться возможность
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// Дата и время остановки бота
        /// Если null то будет работать всегда, пока не остановится принудительно 
        /// оператором или использующей его стратегией торговли
        /// /summary>
        public DateTime? StopTime { get; set; }

        /// <summary>
        /// Токен для остановки задачи Task, в которой запущена тактика.
        /// </summary>
        public CancellationTokenSource CancellationTokenSource { get; set; }
    }

   
}
