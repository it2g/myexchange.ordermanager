﻿using System;
using Abp.Application.Services;
using MyExchange.Api.Entity;

namespace MyExchange.Api.BotTactic
{
    /// <summary>
    /// Строит объект тактики бота на основе переданного контекста и типа тактики.
    /// В системе заложено сопоставление того, каким тактикам, какие контексты соответсвуют
    /// </summary>

    public interface IBotTacticBuilder : IApplicationService
    {
        IBotTactic Create(Type botTacticsType,  BotTacticContextBase botTacticsContext);
    }
}