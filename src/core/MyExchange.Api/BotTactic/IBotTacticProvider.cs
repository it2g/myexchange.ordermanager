﻿using System;
using Abp.Application.Services;

namespace MyExchange.Api.BotTactic
{
    /// <summary>
    /// Предоставляет тактику торговли по идентификатору
    /// </summary>
    public interface IBotTacticProvider : IApplicationService
    {
        /// <summary>
        /// Находит в контейнере зависимостей тактику, в соответсвии с идентификатором
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IBotTactic Get(Guid id);

        /// <summary>
        /// Создание 
        /// </summary>
        /// <typeparam name="TBotTactic"></typeparam>
        /// <param name="botTactic"></param>
        /// <param name="botTacticContext"></param>
        /// <returns></returns>
        TBotTactic CreateBotTactic<TBotTactic>(TBotTactic botTactic, BotTacticContextBase botTacticContext);
    }
}
