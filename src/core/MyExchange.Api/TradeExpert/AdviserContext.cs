﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.TradeExpert
{
    public class AdviserContext 
    {
        public Guid PairId { get; set; }

        public string ExchangeId { get; set; }

        /// <summary>
        /// Временной интервал свечей в размерности которых работает конкретный экземпляр эксперта
        /// </summary>
        public TimeSpan TimeFrame { get; set; }
    }
}
