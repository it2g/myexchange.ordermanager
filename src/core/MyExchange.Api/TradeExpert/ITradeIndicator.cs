﻿using System;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;

namespace MyExchange.Api.TradeExpert
{
    /// <summary>
    /// Эксперт в области торговли основанный на одном из устойчивых паттернов
    /// </summary>
    public interface ITradeIndicator: IBotTactic
    {
        Task CorrectVeracityAsync(SignalConfirmation signalConfirmation);
    }
    
    public interface ITradeIndicator<TContext>: IBotTactic<TContext>, ITradeIndicator
        where TContext : BotTacticContextBase
    {
    }

    /// <summary>
    /// Подтверждение сигнала после проверки его анализатором
    /// </summary>
    public class SignalConfirmation
    {
        public Dto.Signal.SignalDto Signal { get; set; }

        public bool TrendConfirmed { get; set; }
    }
}
