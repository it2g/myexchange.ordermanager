﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;

namespace MyExchange.Api.TradeExpert
{
    /// <summary>
    /// Эксперт в области торговли основанный на одном из устойчивых паттернов
    /// </summary>
    ///
    public interface ITradeAdviser //: IApplicationService
    {
        /// <summary>
        /// Перечень типов стратегий, для которых наиболее приспособлен
        /// эксперт и может давать советы для торговых тактик
        /// </summary>
        IList<TradingTypes> TradingTypeList { get; }

        Task<TradeExpertAdvice> GetAdviceAsync(AdviserContext context);
    }
    public interface ITradeAdviser<TContext> : ITradeAdviser
        where TContext : AdviserContext
    {
        TContext Context { get; set; }
        /// <summary>
        /// Перечень типов стратегий, для которых наиболее приспособлен
        /// эксперт и может давать советы для торговых тактик
        /// </summary>
        IList<TradingTypes> TradingTypeList { get; }

        //Task<TradeExpertAdvice> GetAdviceAsync(TContext context);
    }
}
