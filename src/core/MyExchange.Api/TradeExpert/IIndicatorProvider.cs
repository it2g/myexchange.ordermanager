﻿using System.Collections.Generic;
using Abp.Application.Services;

namespace MyExchange.Api.TradeExpert
{
    public interface IIndicatorProvider : IApplicationService
    {
        ITradeIndicator Get(string id);

        IList<ITradeIndicator> GetIndicators(string stringIndicators);

        IList<ITradeIndicator> GetAll();
    }
}
