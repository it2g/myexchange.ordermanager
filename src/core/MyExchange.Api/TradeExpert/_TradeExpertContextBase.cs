﻿using System;
using MyExchange.Api.BotTactic;

namespace MyExchange.Api.TradeExpert
{
    /// <summary>
    /// Контекст, которым оперирует эксперт
    ///  </summary>
    public abstract class TradeExpertContextBase : BotTacticContextBase
    {
        public Guid PairId { get; set; }

        public string ExchangeId { get; set; }

        /// <summary>
        /// Временной интервал свечей в размерности которых работает конкретный экземпляр эксперта
        /// </summary>
        public TimeSpan TimeFrame { get; set; }

        /// <summary>
        /// Коэф-т достоверности/правдивости генерируемых сигналов
        /// Значение изменяется от 0 до 1 
        /// </summary>
        public Single Veracity { get; set; }
    }
}
