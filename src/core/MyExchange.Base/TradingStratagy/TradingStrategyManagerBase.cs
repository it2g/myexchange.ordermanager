﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Uow;
using Abp.Json;
using Hangfire;
using JetBrains.Annotations;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;
using MyExchange.Api.Exchange;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;
using MyExchange.Api.TradingStrategy;

namespace MyExchange.Base.Services
{
    /// <summary>
    /// Управляет стратегиями: запускает, останавливает, продолжает работы и т.д.
    /// </summary>
    public abstract  class TradingStrategyManagerBase<TTradingStrategy, TTradingStrategyContext> :
        ApplicationService, ITradingStrategyManager<TTradingStrategy, TTradingStrategyContext>
        
        where TTradingStrategyContext : TradingStrategyContextBase
        where TTradingStrategy : ITradingStrategy<TTradingStrategyContext>
    {
        protected IRepository<TTradingStrategyContext, Guid> ContextRepository { get; }

        protected TradingStrategyManagerBase([NotNull] IRepository<TTradingStrategyContext, Guid> contextRepository)
        {
            ContextRepository = contextRepository ?? throw new ArgumentNullException(nameof(contextRepository));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strategy"></param>
        /// <param name="exchangeStrategy">стратегия, котрая запускает бота</param>
        [UnitOfWork(IsDisabled = true)]
        public virtual async Task Start(TTradingStrategy strategy)
        {
            strategy.Context.Status = BotStatuses.Work;
            strategy.Context.StartTime = DateTime.UtcNow;
            strategy.Context.CancellationTokenSource = new CancellationTokenSource();

            //TODO сохранить tokenSource  в контексте, чтобы потом воспользоваться им для
            //остановки задачи
            await ContextRepository.InsertOrUpdateAsync(strategy.Context);
            
            strategy.Execute();
        }
        
        public virtual async Task Stop(TTradingStrategy strategy)
        {
            //bot.Dispose();
            strategy.Context.CancellationTokenSource.Cancel();
            
            strategy.Context.Status = BotStatuses.Stoped;

            await ContextRepository.UpdateAsync(strategy.Context);
        }

        public virtual async Task Pause(TTradingStrategy strategy)
        {
            /// 1. Все установленные заказы на покупку продажу на бирже остаются
            ///но бот уже не сможет делать новые заказы
 

           //2. Удалить бота из очереди исполнения задач

            /// 3. Изменить статус бота и произвести запись этих изменений в БД
            strategy.Context.Status = BotStatuses.OnPause;

            await ContextRepository.UpdateAsync(strategy.Context);
        }

        /// <summary>
        /// Продолжение работы приостановленного ранее бота
        /// </summary>
        /// <param name="strategy"></param>
        public virtual async Task Resume(TTradingStrategy strategy)
        {
            /// 1. Все установленные заказы на покупку продажу на бирже остаются
            ///но бот уже не сможет делать новые заказы


            //2. Постановка в очередь исполнения задач
            //var botTactics = BotTacticProvider.CreateBotTactic(bot.GetType(), bot.Context);

            if (strategy.Context.StartTime == null)
            {
               //  _queryManager.Enqueue(() => botTactics.Execute());
            }
            else
            {
               // _queryManager.Schedule(() => botTactics.Execute(), DateTime.Now - (DateTime)botTactic.StartTime);
            }

            /// 3. Изменить статус бота и произвести запись этих изменений в БД
            strategy.Context.Status = BotStatuses.Work;

            await ContextRepository.UpdateAsync(strategy.Context);
        }

        public virtual IList<TTradingStrategy> Restore()
        {
            throw new NotImplementedException();
        }

        public virtual Task ChangeValue(TTradingStrategy strategy, decimal value)
        {
            throw new NotImplementedException();
        }

    }
}
