﻿using System;
using Abp.Application.Services;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;

namespace MyExchange.Base.BotTactic
{
    /// <summary>
    /// Описание класса и его публичных методов смотри в интерфейсе
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public abstract class BotTacticBase<TContext> : ApplicationService, IBotTactic<TContext>
        where TContext : BotTacticContextBase
    {
        public string Id { get; }

        public TContext Context { get; set; }

        public abstract string Name { get; }

        public abstract Action Execute { get; }

        public abstract void Dispose();

        protected KafkaTopics KafkaTopics { get; }

        public BotTacticBase()
        {
            Id = this.GetType().Name;
            KafkaTopics = new KafkaTopics();
        }
    }
}
