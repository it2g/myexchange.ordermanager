﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.TradingStrategy;
using MyExchange.Api.TradingTactic;

namespace MyExchange.Base.BotTactic
{
    /// <summary>
    /// Описание класса и его публичных методов смотри в интерфейсе
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public abstract class TradingTacticBase<TContext> : BotTacticBase<TContext>, ITradingTactic<TContext>
        where TContext : TradingTacticContextBase
    {
        protected TradingStrategyContextBase TradingStrategyContext;

        public TradingTacticBase(ITradingStrategyContextProvider tradingStrategyContextProvider)
        {
            TradingStrategyContext = tradingStrategyContextProvider.Get(Context.TradingStrategyContextId, Context.TradingStrategyId);
        }

    public virtual void ChangeCurrencyValue(Currency currency, decimal value, string purpose)
        {
            ///TODO Зафиксировать в БД изменения в распределении средств.
            ///Возможно что это лучше делать "Бухгалтеру"
        }
    }
}
