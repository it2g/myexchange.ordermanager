﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Uow;
using Abp.Json;
using Hangfire;
using JetBrains.Annotations;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;
using MyExchange.Api.Exchange;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;

namespace MyExchange.Base.Services
{
    /// <summary>
    /// Управляет ботами: запускает, останавливает, продолжает работы и т.д.
    /// </summary>
    public abstract  class TradingTacticManagerBase<TBotTactic, TBotTacticContext> : ApplicationService, IBotManager<TBotTactic, TBotTacticContext>
        where TBotTacticContext : BotTacticContextBase
        where TBotTactic : IBotTactic<TBotTacticContext>
    {
        protected IBotTacticProvider BotTacticProvider { get; }
        protected IExchangeProvider ExchangeProvider { get; }
        protected IBackgroundJobManager BackgroundJobManager { get; }
        protected IRepository<TBotTacticContext, Guid> ContextRepository { get; }

        protected TradingTacticManagerBase([NotNull]  IBotTacticProvider botTacticProvider,
            [NotNull] IExchangeProvider exchangeProvider,
            [NotNull] IBackgroundJobManager backgroundJobManager,
            [NotNull] IRepository<TBotTacticContext, Guid> contextRepository)
        {
            ExchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            BotTacticProvider = botTacticProvider ?? throw new ArgumentNullException(nameof(botTacticProvider));
            BackgroundJobManager = backgroundJobManager ?? throw new ArgumentNullException(nameof(backgroundJobManager));
            ContextRepository = contextRepository ?? throw new ArgumentNullException(nameof(contextRepository));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="botTactic"></param>
        /// <param name="exchangeStrategy">стратегия, котрая запускает бота</param>
        [UnitOfWork(IsDisabled = true)]
        public virtual async Task Start(TBotTactic botTactic)
        {
            botTactic.Context.Status = BotStatuses.Work;
            botTactic.Context.StartTime = DateTime.UtcNow;
            botTactic.Context.CancellationTokenSource = new CancellationTokenSource();

            //TODO сохранить tokenSource  в контексте, чтобы потом воспользоваться им для
            //остановки задачи
            await ContextRepository.InsertOrUpdateAsync(botTactic.Context);
            
            botTactic.Execute();
        }
        
        public virtual async Task StopBot(TBotTactic bot)
        {
            //bot.Dispose();
            bot.Context.CancellationTokenSource.Cancel();
            
            bot.Context.Status = BotStatuses.Stoped;

            await ContextRepository.UpdateAsync(bot.Context);
        }

        public virtual async Task PauseBot(TBotTactic bot)
        {
            /// 1. Все установленные заказы на покупку продажу на бирже остаются
            ///но бот уже не сможет делать новые заказы
 

           //2. Удалить бота из очереди исполнения задач

            /// 3. Изменить статус бота и произвести запись этих изменений в БД
            bot.Context.Status = BotStatuses.OnPause;

            await ContextRepository.UpdateAsync(bot.Context);
        }

        /// <summary>
        /// Продолжение работы приостановленного ранее бота
        /// </summary>
        /// <param name="bot"></param>
        public virtual async Task ResumeBot(TBotTactic bot)
        {
            /// 1. Все установленные заказы на покупку продажу на бирже остаются
            ///но бот уже не сможет делать новые заказы


            //2. Постановка в очередь исполнения задач
            var botTactics = BotTacticProvider.CreateBotTactic(bot.GetType(), bot.Context);

            if (bot.Context.StartTime == null)
            {
               //  _queryManager.Enqueue(() => botTactics.Execute());
            }
            else
            {
               // _queryManager.Schedule(() => botTactics.Execute(), DateTime.Now - (DateTime)botTactic.StartTime);
            }

            /// 3. Изменить статус бота и произвести запись этих изменений в БД
            bot.Context.Status = BotStatuses.Work;

            await ContextRepository.UpdateAsync(bot.Context);
        }

        public virtual IList<TBotTactic> RestoreBots()
        {
            throw new NotImplementedException();
        }

        public virtual Task ChangeValue(TBotTactic bot, decimal value)
        {
            throw new NotImplementedException();
        }

    }

    //public abstract class TradingTacticManagerBase<TBotTactic, TBotTacticContext, TRepository> : TradingTacticManagerBase<TBotTactic, TBotTacticContext>
    //    where TBotTacticContext : BotTacticContextBase
    //    where TBotTactic : IBotTactic<TBotTacticContext>
    //    where TRepository : IRepository<TBotTacticContext, Guid>

    //{

    //}
}
