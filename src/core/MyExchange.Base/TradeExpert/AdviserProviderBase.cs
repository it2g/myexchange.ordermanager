﻿using System.Collections.Generic;
using Abp.Application.Services;
using MyExchange.Api.TradeExpert;

namespace MyExchange.Web.TradeExpert
{
   public abstract class AdviserProviderBase : ApplicationService, IAdviserProvider
   {
       public abstract IList<ITradeAdviser> GetAdvisers(IDictionary<string, object> parametrs);
   }
}
