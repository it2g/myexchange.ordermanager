﻿using System;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using JetBrains.Annotations;
using MyExchange.Api.Entity;
using MyExchange.Api.TradeExpert;
using MyExchange.Base.BotTactic;
using MyExchange.Web.TradeExpert;

namespace MyExchange.Base.TradeExpert
{
    /// <summary>
    /// Базовый класс для экспертов работающих постоянно в фоновом режиме (индикаторов)
    /// </summary>
    public abstract class TradeIndicatorBase<TContext> : BotTacticBase<TContext>,  ITradeIndicator<TContext>
        where TContext : TradeExpertContextBase
    {
        private static object readLocker = new object();
        private static object writeLocker = new object();
        private IRepository<TContext, Guid> _repository;

        public virtual string BriefName { get; protected set; }

        public virtual string FullName { get; protected set; }

        public virtual Uri Url { get; protected set; }

        public virtual string Description { get; protected set; }

        public TradeIndicatorBase([NotNull] IRepository<TContext, Guid> repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));

        }
        public async Task CorrectVeracityAsync(SignalConfirmation signalConfirmation)
        {
            TContext context;
            lock (readLocker)
            {
                context = _repository.FirstOrDefault(signalConfirmation.Signal.IndicatorContextId);
            }

            //TODO логику переделать 
            if (signalConfirmation.TrendConfirmed)
            {
                context.Veracity += (float)0.01;
            }
            else
            {
                context.Veracity -= (float)0.01;
            }

            lock (writeLocker)
            {
                context = _repository.InsertOrUpdate(context);

            }
        }

       
    }
}
