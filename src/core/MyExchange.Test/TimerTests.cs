using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Services.Crud;
using MyExchange.Api.Services.Infrastructure;
using MyExchange.Services.Bl;
using Timer = MyExchange.Services.Infrastructure.Timer;

namespace MyExchange.Test
{
    [TestClass]
    public class TimerTests
    {
        /// <summary>
        /// ��������� ������������� ���������� ������ �� ������������ 
        /// ���������� �������
        /// </summary>
        [TestMethod]
        public void WaitingTest()
        {
            ITimer timer = new Timer((options)=>
            {
                options.RelativeTimeReference = DateTime.UtcNow - TimeSpan.FromHours(5);
                options.Scale = 5;
            });
           
            //������ ������� �� 5 ������ � ������ ����������� ����-�� 5
            Thread.Sleep(timer.SpeedUpInSeconds(25));

            //����� ������ ���� �� 25 ������ ����� ������ ����� ������, ������
            //�� 5 ����� ������ �������� �������
            var time = timer.CurrentTime();
            var expectedResult = (DateTime.UtcNow - TimeSpan.FromHours(5) + TimeSpan.FromSeconds(25));
            //������� ��������� ���� ������
            Assert.IsTrue(time < expectedResult && time > (expectedResult+ TimeSpan.FromMilliseconds(1)));
        }

       
    }
}
