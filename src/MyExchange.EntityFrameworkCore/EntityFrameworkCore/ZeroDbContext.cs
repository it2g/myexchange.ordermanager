﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using MyExchange.Authorization.Roles;
using MyExchange.Authorization.Users;
using MyExchange.MultiTenancy;
using Abp.EntityHistory;
using Abp.Notifications;
using Abp.Authorization.Users;
using Abp.Organizations;
using Abp.Localization;
using Abp.Auditing;
using Abp.Configuration;
using Abp.Authorization.Roles;
using Abp.Authorization;
using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.MultiTenancy;
using Abp.BackgroundJobs;

namespace MyExchange.EntityFrameworkCore
{
    public class ZeroDbContext : AbpZeroDbContext<Tenant, Role, User, ZeroDbContext>
    {
        /* Define a DbSet for each entity of the application */

        public ZeroDbContext(DbContextOptions<ZeroDbContext> options): base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApplicationLanguageText>()
                .Property(p => p.Value)
                .HasMaxLength(10485759); // any integer that is smaller than 10485760
            base.OnModelCreating(modelBuilder);
            //Для автогенерации идентификаторов в сущностях
            //https://www.npgsql.org/efcore/value-generation.html
            //modelBuilder.ForNpgsqlUseIdentityColumns();

            modelBuilder.Entity<EntityChangeSet>().ToTable("entity_change_sets", "abp_zero");
            modelBuilder.Entity<EntityChange>().ToTable("entity_changes", "abp_zero");
            modelBuilder.Entity<NotificationSubscriptionInfo>().ToTable("notification_subscription_infos", "abp_zero");
            modelBuilder.Entity<UserNotificationInfo>().ToTable("user_notification_infos", "abp_zero");
            modelBuilder.Entity<TenantNotificationInfo>().ToTable("tenant_notification_infos", "abp_zero");
            modelBuilder.Entity<UserOrganizationUnit>().ToTable("user_organization_units", "abp_zero");
            modelBuilder.Entity<OrganizationUnit>().ToTable("organization_units", "abp_zero");
            modelBuilder.Entity<ApplicationLanguageText>().ToTable("application_language_texts", "abp_zero");
            modelBuilder.Entity<ApplicationLanguage>().ToTable("application_languages", "abp_zero");
            modelBuilder.Entity<AuditLog>().ToTable("audit_logs", "abp_zero");
            modelBuilder.Entity<Setting>().ToTable("settings", "abp_zero");
            modelBuilder.Entity<UserPermissionSetting>().ToTable("user_permission_settings", "abp_zero");
            modelBuilder.Entity<RolePermissionSetting>().ToTable("role_permission_settings", "abp_zero");
            modelBuilder.Entity<PermissionSetting>().ToTable("permission_settings", "abp_zero");
            modelBuilder.Entity<RoleClaim>().ToTable("role_claims", "abp_zero");
            modelBuilder.Entity<UserToken>().ToTable("user_tokens", "abp_zero");
            modelBuilder.Entity<UserClaim>().ToTable("user_claims", "abp_zero");
            modelBuilder.Entity<UserRole>().ToTable("user_role", "abp_zero");
            modelBuilder.Entity<UserLoginAttempt>().ToTable("user_login_attempts", "abp_zero");
            modelBuilder.Entity<UserLogin>().ToTable("user_login", "abp_zero");
            modelBuilder.Entity<User>().ToTable("users", "abp_zero");
            modelBuilder.Entity<Role>().ToTable("roles", "abp_zero");
            modelBuilder.Entity<EntityPropertyChange>().ToTable("entity_property_changes", "abp_zero");
            modelBuilder.Entity<Tenant>().ToTable("tenants", "abp_zero");
            modelBuilder.Entity<Edition>().ToTable("editions", "abp_zero");
            modelBuilder.Entity<FeatureSetting>().ToTable("feature_settings", "abp_zero");
            modelBuilder.Entity<TenantFeatureSetting>().ToTable("tenant_feature_settings", "abp_zero");
            modelBuilder.Entity<EditionFeatureSetting>().ToTable("edition_feature_settings", "abp_zero");
            modelBuilder.Entity<BackgroundJobInfo>().ToTable("background_job_infos", "abp_zero");
            modelBuilder.Entity<UserAccount>().ToTable("user_accounts", "abp_zero");
            modelBuilder.Entity<NotificationInfo>().ToTable("notification_infos", "abp_zero");
            modelBuilder.Entity<OrganizationUnitRole>().ToTable("organization_unit_role", "abp_zero");




        }
    
    }
}
