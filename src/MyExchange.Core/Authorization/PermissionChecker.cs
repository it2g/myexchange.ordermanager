﻿using Abp.Authorization;
using MyExchange.Authorization.Roles;
using MyExchange.Authorization.Users;

namespace MyExchange.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
