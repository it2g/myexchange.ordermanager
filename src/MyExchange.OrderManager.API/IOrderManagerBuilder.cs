﻿using Abp.Application.Services;

namespace MyExchange.OrderManager.Api
{
    /// <summary>
    /// Создает анализатор и передает ему контекст
    /// </summary>
    public interface IOrderManagerBuilder : IApplicationService
    {
        IOrderManager Create(OrderManagerContext context);
    }

}