﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Domain.Repositories;

namespace MyExchange.OrderManager.Api.Dal
{
    /// <summary>
    /// Задача провайдера обеспечить подготовку контекста и предварительное сохранение его настроек
    /// где либо. Скорее всего это будут настройки приложения, потому как хранить в таблице БД всего одну строку не целесообразно.
    /// Хотя, если хранить контекст каждого нового запуска отдельно, то для накопления хронологии запусков это может быть целесообразно.
    /// TODO вобщем нужно подумать 
    /// </summary>
    public interface IOrderManagerContextProvider : IApplicationService
    {
        Task<OrderManagerContext> GetContextAsync();
    }
}