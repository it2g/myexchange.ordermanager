﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyExchange.OrderManager.Api
{
    public interface ITrackedOrdersContainer : IOrdersContainer<TrackedOrder>
    {
        IList<TrackedOrder> GetAll();

        /// <summary>
        /// Пересчет Stop-Loss и Take-Profit ордеров
        /// </summary>
        /// <param name="timeFrames"></param>
        void ReCalculate(IEnumerable<int> timeFrames);

        /// <summary>
        /// Проверка ордеров на срабатывание стоп лоссов и тейк-профитов
        /// </summary>
        /// <returns></returns>
        Task<IList<TrackedOrder>> OrderActivationCheck();

    }
}