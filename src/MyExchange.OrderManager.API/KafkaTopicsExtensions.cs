﻿using System;
using System.Collections.Generic;
using System.Text;
using MyExchange.Api.Constants;

namespace MyExchange.OrderManager.Api
{
    /// <summary>
    /// Дополняется состав топиков
    /// </summary>
    public static class KafkaTopicsExtensions
    {
        /// <summary>
        /// Название топика, который будет помечать события формирования ордеров
        /// для передачи их менеджеру ордеров
        /// </summary>
        public static string GetEventBusTopicForAccountManager(this KafkaTopics _) => "order_for_account_manager";
    }
}