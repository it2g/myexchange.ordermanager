﻿using MyExchange.Api.BotTactic;
using MyExchange.Api.TradeExpert;

namespace MyExchange.OrderManager.Api
{
    /// <summary>
    /// Менеджер ордеров принимает ордера и управляет ими в
    /// плоть до выстиавлегния на биржу
    /// </summary>
    public interface IOrderManager : IBotTactic<OrderManagerContext>
    {

    }
}