﻿using Abp.Application.Services;
using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyExchange.OrderManager.Api
{
    public interface IOrdersContainer<T> : IApplicationService, ISingletonDependency
    {
        Task Add(T order);

        Task Remove(T order);

        int Count();

        Task CheckOrders();
    }
}