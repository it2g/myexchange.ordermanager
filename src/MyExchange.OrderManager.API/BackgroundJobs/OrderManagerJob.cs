﻿using System;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Hangfire;
using JetBrains.Annotations;
using MyExchange.OrderManager.Api.Dal;

namespace MyExchange.OrderManager.Api.BackgroundJobs
{
    /// <summary>
    /// Запуск бота менеджера ордеров
    /// </summary>
    [Queue("order_manager")]
    public class OrderManagerJob : BackgroundJob<OrderManagerJobArgs>, ITransientDependency
    {
        private readonly IOrderManagerContextProvider _orderManagerContextProvider;
        private readonly IOrderManagerBuilder _orderManagerBuilder;


        public OrderManagerJob([NotNull] IOrderManagerContextProvider orderManagerContextProvider,
            [NotNull] IOrderManagerBuilder orderManagerBuilder)
        {
            _orderManagerContextProvider = orderManagerContextProvider ?? throw new ArgumentNullException(nameof(orderManagerContextProvider));
            _orderManagerBuilder = orderManagerBuilder ?? throw new ArgumentNullException(nameof(orderManagerBuilder));
        }

        [UnitOfWork(IsDisabled = true)]
        public override void Execute(OrderManagerJobArgs value)
        {
            var orderManagerContext = _orderManagerContextProvider.GetContextAsync().Result;

            var orderManager = _orderManagerBuilder.Create(orderManagerContext);

            orderManager.Execute();
        }
    }

    public class OrderManagerJobArgs
    {

    }
}