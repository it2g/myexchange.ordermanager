﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using MyExchange.Api.Dto.OpenedOrder;

namespace MyExchange.OrderManager.Api
{

    public interface IPlacedOrdersContainer : IOrdersContainer<PlacedOrder>
    {
        IList<PlacedOrder> GetAll();

        /// <summary>
        /// Механизм отслеживания выставленных ордеров на бирже
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<OpenedOrderDto>> CheckOrderForSendingAccountManagerAsync();
    }
}