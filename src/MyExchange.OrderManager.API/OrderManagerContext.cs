﻿using System;
using System.Threading;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Constants;

namespace MyExchange.OrderManager.Api
{
    /// <summary>
    /// Контекст данных, используемый при работе менеджера ордеров
    /// </summary>
    public class OrderManagerContext : BotTacticContextBase
    {

        public ITrackedOrdersContainer TrackedOrdersContainer { get; set; }

        public IPlacedOrdersContainer PlacedOrdersContainer { get; set; }
    }



}