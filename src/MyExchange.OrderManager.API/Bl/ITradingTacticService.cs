﻿using System;
using System.Collections.Generic;
using Abp.Application.Services;
using MyExchange.Api.Dto.OpenedOrder;

namespace MyExchange.OrderManager.API.Bl
{
    public interface ITradingTacticService : IApplicationService
    {
        IEnumerable<OpenedOrderDto> GetTradesTradingTactic(Guid tradingTacticContextId);
    }
}
