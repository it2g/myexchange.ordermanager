﻿using Abp.Application.Services.Dto;

namespace MyExchange.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

