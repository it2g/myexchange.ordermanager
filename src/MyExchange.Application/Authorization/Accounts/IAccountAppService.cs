﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MyExchange.Authorization.Accounts.Dto;

namespace MyExchange.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
