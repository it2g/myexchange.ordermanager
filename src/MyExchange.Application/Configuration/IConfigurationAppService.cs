﻿using System.Threading.Tasks;
using MyExchange.Configuration.Dto;

namespace MyExchange.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
