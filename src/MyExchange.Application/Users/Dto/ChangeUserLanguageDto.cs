using System.ComponentModel.DataAnnotations;

namespace MyExchange.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}